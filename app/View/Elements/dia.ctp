<div class="row">
    <div class="col-sm-5" style="padding-left: 0px; padding-right: 0px;">
        <div class="col-sm-12 infobox-container">
            <div class="infobox infobox-<?php echo ($consolidado['percentual'] > 0) ? "green" : "red" ; ?>">
                    <div class="infobox-icon">
                            <i class="ace-icon fa fa-line-chart"></i>
                    </div>

                    <div class="infobox-data">
                            <span class="infobox-data-number" data-rel="tooltip" data-placement="left" data-original-title="Operações Realizadas"><?php echo $consolidado['nuOperacoes']; ?></span>
                            <div class="infobox-content"><b>PnL: $<?php echo number_format( $consolidado['pnl'], 2, ',', '.'); ?></b></div>
                    </div>

                    <!-- #section:pages/dashboard.infobox.stat -->
                    <div class="stat stat-<?php echo ($consolidado['percentual'] > 0) ? "success" : "important" ; ?>"><?php echo ($consolidado['percentual'] > 0) ? $consolidado['percentual'] : ($consolidado['percentual']*-1); ?>%</div>

                    <!-- /section:pages/dashboard.infobox.stat -->
            </div>
    <!--        <div class="infobox infobox-orange2">
                     #section:pages/dashboard.infobox.sparkline 
                    <div class="infobox-chart">
                            <span class="sparkline" data-values="< ?php echo $consolidado['valores']; ?>">&nbsp;</span>
                    </div>
                     /section:pages/dashboard.infobox.sparkline 

                    <div class="infobox-data">
                            <span class="infobox-data-number">< ?php echo number_format( $consolidado['pnl'], 2, ',', '.'); ?></span>
                            <div class="infobox-content">PnL</div>
                    </div>
            </div>-->
            <div class="infobox infobox-blue2">
                    <div class="infobox-progress">
                            <!-- #section:pages/dashboard.infobox.easypiechart -->
                            <div class="easy-pie-chart percentage" data-percent="<?php echo $consolidado['taxa_acerto']; ?>" data-size="46">
                                    <span class="percent"><?php echo number_format( $consolidado['taxa_acerto'], 0, ',', '.'); ?></span>%
                            </div>

                            <!-- /section:pages/dashboard.infobox.easypiechart -->
                    </div>

                    <div class="infobox-data">
                            <span class="infobox-text"><?php echo $titulo_acerto; ?></span>
                            <div class="infobox-content">
                                <b>
                                    <span class="text-success"><?php echo $consolidado['qtd_gain']; ?> <i class="ace-icon fa fa-thumbs-o-up"></i></span> / 
                                    <span class="red"><?php echo $consolidado['qtd_loss']; ?> <i class="ace-icon fa fa-thumbs-o-down"></i></span>
                                </b>
                            </div>
                    </div>
            </div>
        </div>
        <div class="col-sm-12">
                <div class="widget-box transparent">
                        <div class="widget-body">
                            <div id="evolucao_<?php echo $grafico; ?>" style="min-width: 170px; height: 170px; margin: 0 auto"></div>
                        </div><!-- /.widget-body -->
                </div><!-- /.widget-box -->
        </div><!-- /.col -->

    </div>
    <div class="col-sm-7" style="padding-left: 0px; padding-right: 0px;">
        <div id="<?php echo $grafico; ?>" style="min-width: 220px; height: 220px; margin: 0 auto"></div>
    </div>
</div>

<script type="text/javascript">
    
    Highcharts.setOptions({
        lang: {
            months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            loading: ['Atualizando o gráfico...aguarde'],
            contextButtonTitle: 'Exportar gráfico',
            decimalPoint: ',',
            thousandsSep: '.',
            downloadJPEG: 'Baixar imagem JPEG',
            downloadPDF: 'Baixar arquivo PDF',
            downloadPNG: 'Baixar imagem PNG',
            downloadSVG: 'Baixar vetor SVG',
            printChart: 'Imprimir gráfico',
            rangeSelectorFrom: 'De',
            rangeSelectorTo: 'Para',
            rangeSelectorZoom: 'Zoom',
            resetZoom: 'Limpar Zoom',
            resetZoomTitle: 'Voltar Zoom para nível 1:1',
            drillUpText: 'Voltar',
        }
    });
    
    var saldos_<?php echo $grafico; ?> = [<?php echo $consolidado['valores']; ?>]

    Highcharts.chart('evolucao_<?php echo $grafico; ?>', {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: false
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'area',
            name: 'PnL',
            data: saldos_<?php echo $grafico; ?>
        }]
    });

    Highcharts.chart('<?php echo $grafico; ?>', {
        chart: {
            type: 'pie',
            margin: [0, 0, 0, 0]
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                }
            }
        },
        colors: ['#f7a35c', '#90ee7e', 'blue', 'red', 'orange', 'green', 'purple', 'brown'],
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> do total<br/>'
        },
        series: [{
            name: 'Operações',
            colorByPoint: true,
            data: [{
                name: 'Short',
                y: <?php echo number_format( $consolidado['pct_short'], 2, '.', ','); ?>,
                drilldown: 'Short'
            }, {
                name: 'Long',
                y: <?php echo number_format( $consolidado['pct_long'], 2, '.', ','); ?>,
                drilldown: 'Long'
            }]
        }],
        drilldown: {
            series: [{
                name: 'Short',
                id: 'Short',
                data: [
                    <?php foreach ($consolidado['setup_short'] as $key => $setup) { echo "['" . $categorias[$key] . "', " . number_format( ( (100 * $setup) / $consolidado['short'] ), 2, '.', ',') . "],"; } ?>
                ]
            }, {
                name: 'Long',
                id: 'Long',
                data: [
                    <?php foreach ($consolidado['setup_long'] as $key => $setup) { echo "['" . $categorias[$key] . "', " . number_format( ( (100 * $setup) / $consolidado['long']), 2, '.', ',') . "],"; } ?>
                ]
            }]
        }
    });
    
    
</script>