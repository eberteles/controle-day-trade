<br>
<div class="row">
        <div class="col-xs-12">
            <?php
                $colunas    = array( 
                    array(
                        'nome' => false,
                        'dominio' => 'Conta',
                        'coluna' => 'usuario_id',
                        'tipo' => 'hidden',
                        'data' => AuthComponent::user('id')
                    ),
                    array(
                        'nome' => 'Sigla',
                        'dominio' => 'Conta',
                        'coluna' => 'sigla',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Descrição',
                        'dominio' => 'Conta',
                        'coluna' => 'descricao',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Meta',
                        'dominio' => 'Conta',
                        'coluna' => 'meta',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Saldo',
                        'dominio' => 'Conta',
                        'coluna' => 'saldo',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Acumulado',
                        'dominio' => 'Conta',
                        'coluna' => 'acumulado',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'DBP',
                        'dominio' => 'Conta',
                        'coluna' => 'dbp',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Comissão',
                        'dominio' => 'Conta',
                        'coluna' => 'comissao',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Comissão Mínima',
                        'dominio' => 'Conta',
                        'coluna' => 'comissao_minima',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Taxa',
                        'dominio' => 'Conta',
                        'coluna' => 'taxa',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Risco',
                        'dominio' => 'Conta',
                        'coluna' => 'risco',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Favorita',
                        'dominio' => 'Conta',
                        'coluna' => 'favorita',
                        'tipo' => 'dolar'
                    ),
                    array(
                        'nome' => 'Alteração',
                        'dominio' => 'Conta',
                        'coluna' => 'modified',
                        'tipo' => 'datetime',
                        'input' => false
                    )
                );
                echo $this->Tabela->imprimir($colunas, $contas, 'Conta', array('validationErrors'=>$this->validationErrors, 'threaded'=>true) );
            ?>
        </div>
</div>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>