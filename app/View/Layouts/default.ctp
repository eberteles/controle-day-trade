<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>YegTrading</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
                
                // bootstrap & fontawesome
                echo $this->Html->css('bootstrap');
                //echo $this->Html->css('bootstrap-modal-bs3patch');
                echo $this->Html->css('font-awesome');
                
                // text fonts
                echo $this->Html->css('ace-fonts');
                
                // ace styles
                echo $this->Html->css('ace');
                echo $this->Html->css('ace-skins');
                
                // ace settings handler
                echo $this->Html->script('ace-extra');
                
                echo $this->Html->css( 'jquery.treegrid' );
                
                echo $this->Html->css( 'chosen' );
                
                echo $this->Html->css( 'colorpicker' );
                
                echo $this->Html->css( 'bootstrap-datepicker3' );
                
                echo $this->Html->css( 'jquery.datetimepicker' );
                
                echo $this->Html->css( 'jquery-ui-autocomplete' );
	?>
</head>
<body class="skin-3">
        
        <!--[if !IE]> -->
        <script type="text/javascript">
                window.jQuery || document.write("<script src='<?php echo $this->base; ?>/js/jquery.js'>"+"<"+"/script>");
        </script>

        <!-- <![endif]-->

        <script type="text/javascript">
                if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $this->base; ?>/js/jquery.mobile.custom.js'>"+"<"+"/script>");
        </script>
        <?php
        echo $this->Html->script( 'bootstrap' );
        
        echo $this->Html->script( 'jquery.treegrid' );
        echo $this->Html->script( 'jquery.treegrid.bootstrap2' );
        echo $this->Html->script( 'jquery.easypiechart' );
        echo $this->Html->script( 'jquery.sparkline' );
//        echo $this->Html->script( 'flot/jquery.flot' );
//        echo $this->Html->script( 'flot/jquery.flot.pie' );
//        echo $this->Html->script( 'flot/jquery.flot.resize' );
                
        echo $this->Html->script( 'ace/elements.scroller' );
        
        echo $this->Html->script( 'ace/elements.scroller' );
        echo $this->Html->script( 'ace/elements.colorpicker' );
        echo $this->Html->script( 'ace/elements.fileinput' );
        echo $this->Html->script( 'ace/elements.typeahead' );
        echo $this->Html->script( 'ace/elements.spinner' );
        echo $this->Html->script( 'ace/elements.treeview' );
        echo $this->Html->script( 'ace/elements.wizard' );
        echo $this->Html->script( 'ace/elements.aside' );
        echo $this->Html->script( 'ace/ace' );
        echo $this->Html->script( 'ace/ace.ajax-content' );
        echo $this->Html->script( 'ace/ace.touch-drag' );
        echo $this->Html->script( 'ace/ace.sidebar' );
        echo $this->Html->script( 'ace/ace.sidebar-scroll-1' );
        echo $this->Html->script( 'ace/ace.submenu-hover' );
        echo $this->Html->script( 'ace/ace.widget-box' );
        echo $this->Html->script( 'ace/ace.settings' );
        echo $this->Html->script( 'ace/ace.settings-rtl' );
        echo $this->Html->script( 'ace/ace.settings-skin' );
        echo $this->Html->script( 'ace/ace.widget-on-reload' );
        echo $this->Html->script( 'ace/ace.searchbox-autocomplete' );
        
        echo $this->Html->script( 'jquery.validate' );
        echo $this->Html->script( 'additional-methods' );
        echo $this->Html->script( 'jquery.metadata' );
        
        echo $this->Html->script( 'chosen.jquery' );
        echo $this->Html->script( 'bootbox' );
        
        echo $this->Html->script( 'bootstrap-colorpicker' );
        
        echo $this->Html->script( 'jquery.maskMoney' );
        
        echo $this->Html->script( 'jquery.maskedinput' );
        
        echo $this->Html->script( 'date-time/bootstrap-datepicker' );

        echo $this->Html->script( 'jquery.datetimepicker.full' );
        
        echo $this->Html->script( 'jquery-ui-autocomplete' );
        
        ?>
    
        <!-- #section:basics/navbar.layout -->
        <div id="navbar" class="navbar navbar-default navbar-fixed-top">
                <script type="text/javascript">
                        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
                </script>

                <div class="navbar-container" id="navbar-container">
                        <!-- #section:basics/sidebar.mobile.toggle -->
                        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                                <span class="sr-only">Alternar</span>

                                <span class="icon-bar"></span>

                                <span class="icon-bar"></span>

                                <span class="icon-bar"></span>
                        </button>

                        <!-- /section:basics/sidebar.mobile.toggle -->
                        <div class="navbar-header pull-left">
                                <!-- #section:basics/navbar.layout.brand -->
                                <a href="#" class="navbar-brand">
                                        <small>
                                                <i class="fa fa-area-chart"></i>
                                                YegTrading
                                        </small>
                                </a>

                                <!-- /section:basics/navbar.layout.brand -->

                                <!-- #section:basics/navbar.toggle -->

                                <!-- /section:basics/navbar.toggle -->
                        </div>
                        
                        <div class="navbar-header pull-left col-md-1">&nbsp;</div>

                        <?php 
                            $conta          = $this->Session->read('conta');
                            $movimentos     = $this->Session->read('movimentos');
                            $posicoes_ano   = $this->Session->read('posicoes_ano');
                            if($conta != false) {
                                $saldo      = number_format( $conta['Conta']['saldo']+$conta['Conta']['acumulado'], 2, '.', '');
                                $objetivo   = number_format( (($conta['Conta']['meta']/100) * $conta['Conta']['saldo'] ), 2, '.', '');
                                $falta      = number_format( $objetivo-$conta['Conta']['acumulado'], 2, '.', '');
                                $gain       = number_format( $falta/$this->Formatacao->diasUteis(date('m'),date('Y'),date('d')), 2, ',', '.');
                                //$loss       = number_format( $saldo/$this->Formatacao->diasUteis(date('m'),date('Y')), 2, ',', '.');
                                $loss       = number_format( ($conta['Conta']['risco']/100)*$saldo, 2, ',', '.');
                                $gainPc     = number_format( ($conta['Conta']['acumulado'] * 100)/$objetivo, 0, '.', '');
                                if($gainPc < 0){ $gainPc = 0; }
                                
                                $sparkline  = '';
                                for($i = count($sparkline)+1; $i < 8; $i++) {
                                    if($sparkline != '') { $sparkline .= ','; }
                                    $sparkline .= 0;
                                }
                                foreach ($movimentos as $movimento) {
                                    if($sparkline != '') { $sparkline .= ','; }
                                    $sparkline .= $movimento[0]['valor'];
                                }
                                
                                $sparkline_ano  = '';
                                $acumulado_ano  = 0;
                                foreach ($posicoes_ano as $posicao) {
                                    if($sparkline_ano != '') { $sparkline_ano .= ','; }
                                    $sparkline_ano .= $posicao[0]['valor'];
                                    $acumulado_ano += $posicao[0]['valor'];
                                }
                                
                                $acerto_ano = number_format($this->Session->read('acerto_ano'), 0, ',', '.');
                        ?>
                        <!-- #section:basics/navbar.dropdown -->
                        <div class="navbar-header pull-left">
                            <div class="infobox infobox-blue infobox-small infobox-dark  hidden-xs hidden-sm hidden-md" style="height: 44px; padding-top: 1px;">
                                <div class="infobox-chart">
                                        <span class="sparkline" data-values="<?php echo $sparkline; ?>"></span>
                                </div>
                                <div class="infobox-data">
                                        <div class="infobox-content">Saldo</div>
                                        <div class="infobox-content"><?php echo number_format( $saldo, 2, ',', '.'); ?></div>
                                </div>
                            </div>
                            <div class="infobox infobox-green infobox-small infobox-dark" style="height: 44px; padding-top: 1px;">
                                <div class="infobox-icon">
                                        <i class="ace-icon fa fa-check"></i>
                                </div>
                                <div class="infobox-data">
                                        <div class="infobox-content" data-rel="tooltip" data-placement="left" data-original-title="Meta de Ganho Diário definido através da sua estratégia.">Meta</div>
                                        <div class="infobox-content"><?php echo $gain; ?></div>
                                </div>
                            </div>
                            <div class="infobox infobox-red infobox-small infobox-dark" style="height: 44px; padding-top: 1px;">
                                <div class="infobox-icon">
                                        <i class="ace-icon fa fa-hand-stop-o"></i>
                                </div>
                                <div class="infobox-data">
                                        <div class="infobox-content"  data-rel="tooltip" data-placement="left" data-original-title="Limite de Perda Diário definido através da sua estratégia.">Limite</div>
                                        <div class="infobox-content"><?php echo $loss; ?></div>
                                </div>
                            </div>
                            <div class="infobox infobox-purple2 infobox-small infobox-dark" style="height: 44px; padding-top: 1px;">
                                <div class="infobox-progress">
                                        <!-- #section:pages/dashboard.infobox.easypiechart -->
                                        <div class="easy-pie-chart percentage" data-percent="<?php echo $gainPc; ?>" data-size="39">
                                                <span class="percent"><?php echo $gainPc; ?></span>%
                                        </div>

                                        <!-- /section:pages/dashboard.infobox.easypiechart -->
                                </div>
                                <div class="infobox-data">
                                        <div class="infobox-content" data-rel="tooltip" data-placement="left" data-original-title="Pnl Realizado no Mês e Percentual da Meta já atingido.">% Meta</div>
                                        <div class="infobox-content"><?php echo number_format( $conta['Conta']['acumulado'], 2, ',', '.'); ?></div>
                                </div>
                            </div>
                            <div class="infobox infobox-brown infobox-small infobox-dark hidden-xs hidden-sm hidden-md" style="height: 44px; padding-top: 1px;">
                                <div class="infobox-chart">
                                        <span class="sparkline" data-values="<?php echo $sparkline_ano; ?>"></span>
                                </div>
                                <div class="infobox-data">
                                        <div class="infobox-content" data-rel="tooltip" data-placement="left" data-original-title="Pnl Realizado no Ano">Ano</div>
                                        <div class="infobox-content"><?php echo number_format( $acumulado_ano, 2, ',', '.'); ?></div>
                                </div>
                            </div>
                            <div class="infobox infobox-green2 infobox-small infobox-dark hidden-xs hidden-sm hidden-md" style="height: 44px; padding-top: 1px;">
                                <div class="infobox-progress">
                                        <!-- #section:pages/dashboard.infobox.easypiechart -->
                                        <div class="easy-pie-chart percentage" data-percent="<?php echo $acerto_ano; ?>" data-size="39">
                                                <span class="percent"><?php echo $acerto_ano; ?></span>%
                                        </div>

                                        <!-- /section:pages/dashboard.infobox.easypiechart -->
                                </div>
                                <div class="infobox-data">
                                        <div class="infobox-content" data-rel="tooltip" data-placement="left" data-original-title="% de Acerto no Ano">Acerto</div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            }
                        ?>

                        <!-- /section:basics/navbar.dropdown -->
                </div><!-- /.navbar-container -->
        </div>

        <!-- /section:basics/navbar.layout -->
        <div class="main-container" id="main-container">
                <script type="text/javascript">
                        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
                </script>

                <!-- #section:basics/sidebar -->
                <div id="sidebar" class="sidebar sidebar-fixed responsive">
                        <script type="text/javascript">
                                try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
                        </script>

<!--                        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                                        <button class="btn btn-success">
                                                <i class="ace-icon fa fa-signal"></i>
                                        </button>

                                        <button class="btn btn-info">
                                                <i class="ace-icon fa fa-pencil"></i>
                                        </button>

                                         #section:basics/sidebar.layout.shortcuts 
                                        <button class="btn btn-warning">
                                                <i class="ace-icon fa fa-users"></i>
                                        </button>

                                        <button class="btn btn-danger">
                                                <i class="ace-icon fa fa-cogs"></i>
                                        </button>

                                         /section:basics/sidebar.layout.shortcuts 
                                </div>

                                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                                        <span class="btn btn-success"></span>

                                        <span class="btn btn-info"></span>

                                        <span class="btn btn-warning"></span>

                                        <span class="btn btn-danger"></span>
                                </div>
                        </div> /.sidebar-shortcuts -->

                        <ul class="nav nav-list">
                                <li class="<?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'index'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/dashboard">
                                                <i class="menu-icon fa fa-pie-chart"></i>
                                                <span class="menu-text"> Resumo </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <li class="<?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'dia'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/dashboard/dia">
                                                <i class="menu-icon fa fa-calendar-check-o"></i>
                                                <span class="menu-text"> Dia </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <li class="<?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'mes'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/dashboard/mes">
                                                <i class="menu-icon fa fa-calendar-check-o"></i>
                                                <span class="menu-text"> Mês </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <li class="<?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'ano'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/dashboard/ano">
                                                <i class="menu-icon fa fa-calendar-check-o"></i>
                                                <span class="menu-text"> Ano </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <?php if($conta != false) { ?>
                                <li class="<?php if ($this->params['controller'] == 'calculadora'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/calculadora">
                                                <i class="menu-icon fa fa-calculator"></i>
                                                <span class="menu-text"> Calculadora </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <?php } ?>
                                <li class="<?php if ($this->params['controller'] == 'entradas'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/entradas">
                                                <i class="menu-icon fa fa-arrow-right"></i>
                                                <span class="menu-text"> Entradas </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <li class="<?php if ($this->params['controller'] == 'categorias'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/categorias">
                                                <i class="menu-icon fa fa-laptop"></i>
                                                <span class="menu-text"> Setups </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <li class="<?php if ($this->params['controller'] == 'contas'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/contas">
                                                <i class="menu-icon fa fa-money"></i>
                                                <span class="menu-text"> Contas </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>

                                <?php
                                if(AuthComponent::user('role') == 'admin') {
                                ?>
                                <li class="<?php if ($this->params['controller'] == 'usuarios' && $this->params['action'] != 'perfil'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/usuarios">
                                                <i class="menu-icon fa fa-users"></i>
                                                <span class="menu-text"> Usuários </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <?php
                                } else {
                                ?>
                                <li class="<?php if ($this->params['controller'] == 'usuarios' && $this->params['action'] == 'perfil'){echo 'active';}; ?>">
                                        <a href="<?php echo $this->base; ?>/usuarios/perfil">
                                                <i class="menu-icon fa fa-user"></i>
                                                <span class="menu-text"> Perfil </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                                <?php
                                }
                                ?>
                                <li class="">
                                        <a href="<?php echo $this->base; ?>/usuarios/sair">
                                                <i class="menu-icon fa fa-power-off"></i>
                                                <span class="menu-text"> Sair </span>
                                        </a>

                                        <b class="arrow"></b>
                                </li>
                        </ul><!-- /.nav-list -->

                        <!-- #section:basics/sidebar.layout.minimize -->
                        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                                <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                        </div>

                        <!-- /section:basics/sidebar.layout.minimize -->
                        <script type="text/javascript">
                                try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
                        </script>
                </div>

                <!-- /section:basics/sidebar -->
                <div class="main-content">
                        <div class="main-content-inner">
                                <!-- #section:basics/content.breadcrumbs -->
                                <div class="breadcrumbs" id="breadcrumbs">
                                        <script type="text/javascript">
                                                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                                        </script>

                                        <ul class="breadcrumb">
                                                <h4>
                                                    &nbsp;&nbsp;
                                                    <?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'index'){ ?>
                                                            <i class="menu-icon fa fa-pie-chart blue"></i>
                                                            &nbsp; Resumo
                                                    <?php } ?>
                                                            
                                                    <?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'dia'){ ?>
                                                            <i class="menu-icon fa fa-calendar-check-o purple"></i>
                                                            &nbsp; Resumo do Dia
                                                    <?php } ?>
                                                            
                                                    <?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'mes'){ ?>
                                                            <i class="menu-icon fa fa-calendar-check-o blue"></i>
                                                            &nbsp; Resumo do Mês
                                                    <?php } ?>
                                                            
                                                    <?php if ($this->params['controller'] == 'dashboard' && $this->params['action'] == 'ano'){ ?>
                                                            <i class="menu-icon fa fa-calendar-check-o blue"></i>
                                                            &nbsp; Resumo do Ano
                                                    <?php } ?>
                                                            
                                                    <?php if ($this->params['controller'] == 'calculadora'){ ?>
                                                            <i class="menu-icon fa fa-calculator grey"></i>
                                                            &nbsp; Calculadora
                                                    <?php } ?>
                                                        
                                                    <?php if ($this->params['controller'] == 'entradas'){ ?>
                                                        <i class="menu-icon fa fa-arrow-right green"></i>
                                                        &nbsp; Entradas
                                                    <?php } ?>
                                                        
                                                    <?php if ($this->params['controller'] == 'categorias'){ ?>
                                                        <i class="menu-icon fa fa-laptop red2"></i>
                                                        &nbsp; Setups
                                                    <?php } ?>
                                                    <?php if ($this->params['controller'] == 'contas'){ ?>
                                                        <i class="menu-icon fa fa-money grey"></i>
                                                        &nbsp; Contas
                                                    <?php } ?>
                                                        
                                                    <?php 
                                                        if ($this->params['controller'] == 'usuarios'){ 
                                                            if ($this->params['action'] == 'perfil'){ 
                                                    ?>
                                                        <i class="menu-icon fa fa-user red2"></i>
                                                        &nbsp; Perfil
                                                    <?php
                                                            } else {
                                                    ?>
                                                        <i class="menu-icon fa fa-group grey"></i>
                                                        &nbsp; Usuários
                                                    <?php } } ?>
                                                </h4>

<!--                                                <li>
                                                        <a href="#">Other Pages</a>
                                                </li>
                                                <li class="active">Blank Page</li>-->
                                        </ul><!-- /.breadcrumb -->

                                        <!-- #section:basics/content.searchbox -->
<!--                                        <div class="nav-search" id="nav-search">
                                                <form class="form-search">
                                                        <span class="input-icon">
                                                                <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                                                <i class="ace-icon fa fa-search nav-search-icon"></i>
                                                        </span>
                                                </form>
                                        </div> /.nav-search -->

                                        <!-- /section:basics/content.searchbox -->
                                </div>

                                <!-- /section:basics/content.breadcrumbs -->
                                <div class="page-content">
                                        <!-- #section:settings.box -->
<!--                                        <div class="ace-settings-container" id="ace-settings-container">
                                                <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                                                        <i class="ace-icon fa fa-cog bigger-130"></i>
                                                </div>

                                                <div class="ace-settings-box clearfix" id="ace-settings-box">
                                                        <div class="pull-left width-50">
                                                                 #section:settings.skins 
                                                                <div class="ace-settings-item">
                                                                        <div class="pull-left">
                                                                                <select id="skin-colorpicker" class="hide">
                                                                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                                                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                                                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                                                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                                                                </select>
                                                                        </div>
                                                                        <span>&nbsp; Choose Skin</span>
                                                                </div>

                                                                 /section:settings.skins 

                                                                 #section:settings.navbar 
                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                                                                        <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                                                                </div>

                                                                 /section:settings.navbar 

                                                                 #section:settings.sidebar 
                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                                                                        <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                                                                </div>

                                                                 /section:settings.sidebar 

                                                                 #section:settings.breadcrumbs 
                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                                                                        <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                                                                </div>

                                                                 /section:settings.breadcrumbs 

                                                                 #section:settings.rtl 
                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                                                                        <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                                                                </div>

                                                                 /section:settings.rtl 

                                                                 #section:settings.container 
                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                                                                        <label class="lbl" for="ace-settings-add-container">
                                                                                Inside
                                                                                <b>.container</b>
                                                                        </label>
                                                                </div>

                                                                 /section:settings.container 
                                                        </div> /.pull-left 

                                                        <div class="pull-left width-50">
                                                                 #section:basics/sidebar.options 
                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" />
                                                                        <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                                                                </div>

                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" />
                                                                        <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                                                                </div>

                                                                <div class="ace-settings-item">
                                                                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" />
                                                                        <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                                                                </div>

                                                                 /section:basics/sidebar.options 
                                                        </div> /.pull-left 
                                                </div> /.ace-settings-box 
                                        </div> /.ace-settings-container -->

                                        <!-- /section:settings.box -->
                                        <div class="row">
                                                <div class="col-xs-12">
                                                        <!-- PAGE CONTENT BEGINS -->
                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
                                                        
        <div id="modal-form-entrada" class="modal fade" tabindex="-1" data-backdrop="static">
                <div class="modal-dialog">
                    <?php
                        echo $this->Form->create('Entrada', array('id'=>'form-entrada'));
                    ?>
                        <div class="modal-content">
                                <div class="modal-header no-padding">
                                        <div class="table-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                        <span class="white">&times;</span>
                                                </button>
                                            <span id="titulo_form_entrada">Nova Entrada</span>
                                        </div>
                                </div>

                                <div class="modal-body no-padding">
                                    <div class="col-xs-12">
                                        <?php
                                        echo $this->Form->hidden("id");
                                        ?>
                                        <br>
                                    </div>
                                </div>
                                
                                <div class="modal-footer no-margin-top">
                                    <button class="btn btn-sm btn-primary "><i class="ace-icon fa fa-check"></i>Salvar</button>
                                </div>
                        </div><!-- /.modal-content -->
                    <?php
                        echo $this->Form->end();
                    ?>
                </div><!-- /.modal-dialog -->
        </div>
                                                        
                <?php echo $this->element('sql_dump'); ?>
                                                        <!-- PAGE CONTENT ENDS -->
                                                </div><!-- /.col -->
                                        </div><!-- /.row -->
                                </div><!-- /.page-content -->
                        </div>
                </div><!-- /.main-content -->

                <div class="footer">
                        <div class="footer-inner">
                                <!-- #section:basics/footer -->
                                <div class="footer-content">
                                    © <?php echo Date('Y'); ?> YegTrading - <?php echo Configure::version(); ?>
                                </div>
                        <div>
                            
                        </div>

                                <!-- /section:basics/footer -->
                        </div>
                </div>

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
                </a>
        </div><!-- /.main-container -->

        <!-- basic scripts -->
    <script type="text/javascript">
        
        $('[data-rel=tooltip]').tooltip({container:'body'});
        
        $('.easy-pie-chart.percentage').each(function(){
                var $box = $(this).closest('.infobox');
                var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
                var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
                var size = parseInt($(this).data('size')) || 50;
                $(this).easyPieChart({
                        barColor: barColor,
                        trackColor: trackColor,
                        scaleColor: false,
                        lineCap: 'butt',
                        lineWidth: parseInt(size/10),
                        animate: ace.vars['old_ie'] ? false : 1000,
                        size: size
                });
        });
        
        $('.sparkline').each(function(){
                var $box = $(this).closest('.infobox');
                var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
                $(this).sparkline('html',
                                                 {
                                                        tagValuesAttribute:'data-values',
                                                        type: 'bar',
                                                        barColor: barColor ,
                                                        chartRangeMin:$(this).data('min') || 0
                                                 });
        });
                                
    </script>
        
</body>
</html>