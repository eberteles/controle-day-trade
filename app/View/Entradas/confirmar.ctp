<?php
    echo $this->Form->create('Entrada');
    
    $posicoes   = $this->Session->read('posicoesProntas');
    if(!$posicoes[0]['conta']) {
?>

    <div id="modal-form" class="modal fade" tabindex="-1" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header no-padding">
                        <div class="table-header">
<!--                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        <span class="white">&times;</span>
                                </button>-->
                            <span id="titulo_form">Informe os Dados da Nova Conta:</span>
                        </div>
                </div>

                <div class="modal-body no-padding">
                    <div class="col-xs-12">

                        <div class="form-group row">
                          <div class="col-xs-4">
                            <?php echo $this->Form->input('Conta.sigla', array('readonly'=>true, 'label'=>'Conta', 'value'=>$posicoes[0]['nuConta'])); ?>
                          </div>
                          <div class="col-xs-8">
                            <?php echo $this->Form->input('Conta.descricao', array('class'=>'form-control', 'label'=>'Descrição', 'required'=>'required')); ?>
                          </div>
                          <div class="col-xs-2">
                            <?php echo $this->Form->input('Conta.risco', array('type'=>'text', 'class'=>'form-control money', 'value'=>'5,00', 'label'=>'Risco(%)', 'required'=>'required', 'data-rel'=>'tooltip', 'data-original-title'=>'Risco para calcular o Stop.')); ?>
                          </div>
                          <div class="col-xs-3">
                            <?php echo $this->Form->input('Conta.meta', array('type'=>'text', 'class'=>'form-control percent', 'value'=>'50,00', 'label'=>'Meta Mensal(%)', 'required'=>'required')); ?>
                          </div>
                          <div class="col-xs-3">
                            <?php echo $this->Form->input('Conta.saldo', array('type'=>'text', 'class'=>'form-control money', 'label'=>'Deposito Inicial', 'required'=>'required')); ?>
                          </div>
                          <div class="col-xs-4">
                            <?php echo $this->Form->input('Conta.dbp', array('type'=>'text', 'class'=>'form-control dbp', 'value'=>'12', 'label'=>'DBP (Alavancagem)', 'required'=>'required', 'data-rel'=>'tooltip', 'data-original-title'=>'Alavancagem')); ?>
                          </div>
                          <div class="col-xs-4">
                            <?php echo $this->Form->input('Conta.comissao', array('type'=>'text', 'class'=>'form-control percent3', 'value'=>'0,010', 'label'=>'Comissão por Share', 'data-rel'=>'tooltip', 'data-original-title'=>'Comissão cobrada por Share')); ?>
                          </div>
                          <div class="col-xs-4">
                            <?php echo $this->Form->input('Conta.comissao_minima', array('type'=>'text', 'class'=>'form-control money', 'value'=>'1,50', 'label'=>'Comissão Mínima', 'data-rel'=>'tooltip', 'data-original-title'=>'Comissão Mínima por Ticket')); ?>
                          </div>
                          <div class="col-xs-4">
                            <?php echo $this->Form->input('Conta.taxa', array('type'=>'text', 'class'=>'form-control percent3', 'value'=>'0,003', 'label'=>'Taxa por Share', 'data-rel'=>'tooltip', 'data-original-title'=>'Fees cobrada por Share')); ?>
                          </div>
                        </div>
                                        
                    </div>
                </div>

                <div class="modal-footer no-margin-top">
                    <button id="fecharConta" type="button" class="btn btn-sm btn-primary"><i class="ace-icon fa fa-check"></i>Pronto</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<?php
    }
?>

<div class="row">
    <b>Confirme as operações e indique os setups utilizados:</b><br>
    <div>
        <h5><b>Data: <?php echo $this->Formatacao->getDataSemHora($posicoes[0]['Posicao']['created']); ?></b></h5>
    </div>
    <table class="tree table table-striped table-bordered table-hover">
        <thead>
            <tr><th>Conta</th><th>Ação</th><th>Tipo</th><th>P/L</th><th>Comissão</th><th>Taxa</th><th><b>Líquido</b></th><th>Setup</th></tr></thead>
        <tbody>
        <?php
            $total  = 0; $totalBruto  = 0; $totalComissao  = 0; $totalTaxa  = 0;
            foreach ($posicoes as $key => $posicao) {
                $total         += ($posicao['Posicao']['total'] - $posicao['Posicao']['comissao'] - $posicao['Posicao']['taxa']);
                $totalBruto    += $posicao['Posicao']['total'];
                $totalComissao += $posicao['Posicao']['comissao'];
                $totalTaxa     += $posicao['Posicao']['taxa'];
                
                $opcoes = array();
                $opcoes['class']  = 'form-control';
                $opcoes['options']  = $categorias;
                $opcoes['escape']   = false;
                $opcoes['label']    = false;
                $opcoes['required'] = 'required';
                $opcoes['empty']    = 'Selecione...';
        ?>
            <tr class="treegrid-<?php echo $key; ?>" style="background-color: <?php echo ($posicao['Posicao']['resultado']=='P')?'#ccffcc':'#ffcccc'; ?>">
                    <td><?php echo $posicao['nuConta']; ?></td>
                    <td><?php echo $posicao['acao']; ?></td>
                    <td><?php echo ($posicao['Posicao']['tipo']=='S')?'Short':'Long'; ?></td>
                    <td><?php echo $this->Formatacao->moeda($posicao['Posicao']['total']); ?></td>
                    <td><?php echo ($posicao['Posicao']['comissao'] == 0)?'-':$this->Formatacao->moeda($posicao['Posicao']['comissao']); ?></td>
                    <td><?php echo ($posicao['Posicao']['taxa'] == 0)?'-':$this->Formatacao->moeda($posicao['Posicao']['taxa']); ?></td>
                    <td><b><?php echo $this->Formatacao->moeda($posicao['Posicao']['total'] - $posicao['Posicao']['comissao'] - $posicao['Posicao']['taxa']); ?></b></td>
                    <td><?php echo $this->Form->input('Entrada.categoria.'.$key, $opcoes); ?></td>
            </tr>
            <tr class="treegrid-ticket-<?php echo $key; ?> treegrid-parent-<?php echo $key; ?>">
                <td colspan="7">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr><td>Hora</td><td>Shares</td><td>Preço</td><td>Tipo</td></tr></thead>
                        <tbody>
                        <?php
                            foreach ($posicao['Ticket'] as $keyTicket => $ticket) {
                                $color = ($ticket['tipo']=='S')?'#990000':'#003333';
                        ?>
                          <tr>
                            <td style="color: <?php echo $color; ?>"><?php echo $ticket['time']; ?></td>
                            <td style="color: <?php echo $color; ?>"><?php echo $ticket['share']; ?></td>
                            <td style="color: <?php echo $color; ?>"><?php echo $this->Formatacao->moeda($ticket['preco']); ?></td>
                            <td style="color: <?php echo $color; ?>"><?php echo ($ticket['tipo']=='S')?'Short':'Buy'; ?></td>
                          </tr>
                        <?php
                            }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                  <?php 
                    echo $this->Form->input('Entrada.nota.'.$key, array('class'=>'autosize-transition form-control','placeholder'=>'Anotações sobre o trade.', 'rows'=>'5', 'label'=>false));
                    //echo $this->Form->input('Entrada.file.'.$key, array('type'=>'file', 'class'=>'upload', 'label'=>false));
                  ?>
                </td>
            </tr>
        <?php
            }
        ?>
        </tbody>
        
        <thead>
            <tr>
                <th colspan="3">Totais</th>
                <th><?php echo $this->Formatacao->moeda($totalBruto); ?></th>
                <th><?php echo $this->Formatacao->moeda($totalComissao); ?></th>
                <th><?php echo $this->Formatacao->moeda($totalTaxa); ?></th>
                <th><?php echo $this->Formatacao->moeda($total); ?></th>
                <th>&nbsp;</th>
            </tr>
        </thead>
    </table>
    
    <br>
    <center><button type="button" class="btn btn-sm" onClick="history.go(-1)"><i class="ace-icon fa fa-arrow-left"></i>Voltar</button>
    <button type="submit" class="btn btn-sm btn-primary"><i class="ace-icon fa fa-check"></i>Salvar</button></center>
</div>

<?php
    echo $this->Form->end();
?>

<script type="text/javascript">
    
    <?php
    if(!$posicoes[0]['conta']) { ?>
        $('#modal-form').modal('show'); <?php
    }
    ?>
    
    //$('.chosen-select').chosen();

    $('.tree').treegrid({
        initialState: 'collapsed',
        expanderExpandedClass: 'fa fa-minus-circle',
        expanderCollapsedClass: 'fa fa-plus-circle'
    });
    
    $('.tree').find('tr').each(function(){
      if ($(this).treegrid('isFirst')){
            $(this).treegrid('expand');
      }
    });
    
    $('.upload').ace_file_input({
            style: 'well',
            no_file:'Imagem do Trade...',
            btn_choose:'Imagem do Trade...',
            btn_change:'Alterar Imagem...',
            droppable:true,
            onchange:null,
            thumbnail:'small', //| true | large
            whitelist:'gif|png|jpg|jpeg',
            blacklist:'exe|php'
            //onchange:''
            //
    });
    
    $('[data-rel=tooltip]').tooltip({container:'body'});
    
    $(".money").maskMoney({allowNegative: false, thousands:'.', decimal:','});
    $(".percent").maskMoney({allowNegative: false, thousands:'.', decimal:','});
    $(".percent3").maskMoney({allowNegative: false, thousands:'.', decimal:',', precision:3});
    $('.dbp').mask('9?9');
    $("#fecharConta").bind('click', function(e) {
        if($('#ContaDescricao').val() == "" || $('#ContaMeta').val() == "" || $('#ContaSaldo').val() == "" || $('#ContaDbp').val() == "") {
            bootbox.alert({
                size: 'small',
                message: '<span class="text-warning bigger-110 orange"><i class="ace-icon fa fa-exclamation-triangle"></i> <br>Informe os dados obrigatórios da Conta.</span>'
            })
        } else {
            $('#modal-form').modal('hide');
        }
    });
    
</script>