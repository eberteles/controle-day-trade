<br>
<?php
    echo $this->Form->create('Entrada');
?>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-2">
            <?php
                echo $this->Form->input('data', array('class'=>'form-control date-picker', 'required'=>'required', 'value'=>date('d/m/Y')));
            ?>
            </div>
            <div class="col-xs-6">
            <?php
                $opcoes = array();
                $opcoes['class']  = 'form-control chosen-select';
                $opcoes['options']  = array('P'=>'Positiva', 'N'=>'Negativa', '0'=>'Neutra');
                $opcoes['escape']   = false;
                $opcoes['empty']    = 'Selecione...';
                echo $this->Form->input('market', $opcoes);
            ?>
            </div>
            <div class="col-xs-12">
            Exemplo: <pre>08:10:25	CORT	B	23.45	100	AA	TRCOLM698	Buy</pre>
            <?php
                $opcoes = array();
                $opcoes['label']    = false;
                $opcoes['type']     = 'textarea';
                $opcoes['class']    = 'form-control';
                $opcoes['rows']     = '15';
                $opcoes['required'] = 'required';
                echo $this->Form->input('entradas', $opcoes);
            ?>
            </div>
            <div class="col-xs-12 center">
            <button type="submit" class="btn btn-sm btn-primary"><i class="ace-icon fa fa-check"></i>Próximo</button>
            </div>
        </div>
    </div>
</div>

<?php
    echo $this->Form->end();
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>

<script type="text/javascript">

        $('.date-picker').datepicker({
            mask:true,
            format:'dd/mm/yyyy'
        });
        
        $(document).delegate('#EntradaEntradas', 'keydown', function(e) {
          var keyCode = e.keyCode || e.which;

          if (keyCode == 9) {
            e.preventDefault();
            var start = this.selectionStart;
            var end = this.selectionEnd;

            // set textarea value to: text before caret + tab + text after caret
            $(this).val($(this).val().substring(0, start)
                        + "\t"
                        + $(this).val().substring(end));

            // put caret at right position again
            this.selectionStart =
            this.selectionEnd = start + 1;
          }
        });
    
</script>