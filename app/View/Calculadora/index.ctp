    <div class="row">
    <?php
        echo $this->Form->create('Calculadora');
        echo $this->Form->input('saldo', array('type'=>'hidden', 'value'=>$saldo));
        echo $this->Form->input('bp', array('type'=>'hidden', 'value'=>$bp));
        echo $this->Form->input('comissao', array('type'=>'hidden', 'value'=>$comissao));
        echo $this->Form->input('cm_minima', array('type'=>'hidden', 'value'=>$cm_minima));
        echo $this->Form->input('taxa', array('type'=>'hidden', 'value'=>$taxa));
        
        echo $this->Form->input('exposicao', array('class'=>'form-control', 
            'options'=>array('0.0025'=>'0.25%', '0.0050'=>'0.50%', '0.01'=>'1.00%', '0.02'=>'2.00%', '0.025'=>'2.50%', '0.03'=>'3.00%', '0.04'=>'4.00%', 
                '0.05'=>'5.00%', '0.06'=>'6.00%', '0.07'=>'7.00%', '0.08'=>'8.00%', '0.09'=>'9.00%', '0.1'=>'10.00%'), 
            'empty'=>'Selecione...', 'value'=>$risco, 'label'=>'Risco (%)'));
    ?>
    </div>
    <div class="row">
        
        <div class="col-xs-12 col-sm-6 widget-container-col">
            <div class="widget-box widget-color-red2">
                        <!-- #section:custom/widget-box.options -->
                <div class="widget-header">
                        <h5 class="widget-title bigger lighter">
                                <i class="ace-icon fa fa-arrow-down"></i>
                                Short
                        </h5>
                </div>

                        <!-- /section:custom/widget-box.options -->
                <div class="widget-body">
                    <div class="widget-main no-padding">
        
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>Stop</th>
                                    <th>Entrada</th>
                                    <th>Shares</th>
                                    <th>Objetivo</th>
                                    <th>Parcial</th>
                                    <th>Risco</th>
                                    <th>
                                        PnL 
                                        <i class="ace-icon fa fa-question-circle" data-rel="tooltip" data-placement="left" data-original-title="Previsão de Lucro Líquido: (Lucro - Taxas)"></i>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                              <?php
                              for ($i = 1; $i <= 3; $i++) {
                              ?>
                                <tr>
                                    <td>
                                        <?php
                                            echo $this->Form->input('short-stop'.$i, array('type'=>'text', 'label'=>false,'class'=>'form-control input-mini dolar'));
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $this->Form->input('short-entrada'.$i, array('type'=>'text', 'label'=>false,'class'=>'form-control input-mini dolar'));
                                        ?>
                                    </td>
                                    <td id="short-share<?php echo $i; ?>" valign="center" class="no-hover center">0</td>
                                    <td id="short-objetivo<?php echo $i; ?>" valign="center" class="no-hover center">$0.00</td>
                                    <td id="short-parcial<?php echo $i; ?>" valign="center" class="no-hover center">0</td>
                                    <td id="short-risco<?php echo $i; ?>" valign="center" class="center">$0.00</td>
                                    <td id="short-pnl<?php echo $i; ?>" valign="center" class="center">$0.00</td>
                                </tr>
                              <?php
                              }
                              ?>

                             </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.span -->
        
        <div class="col-xs-12 col-sm-6 widget-container-col">
            <div class="widget-box widget-color-green2">
                        <!-- #section:custom/widget-box.options -->
                <div class="widget-header">
                        <h5 class="widget-title bigger lighter">
                                <i class="ace-icon fa fa-arrow-up"></i>
                                Long
                        </h5>
                </div>

                        <!-- /section:custom/widget-box.options -->
                <div class="widget-body">
                    <div class="widget-main no-padding">
        
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>Stop</th>
                                    <th>Entrada</th>
                                    <th>Shares</th>
                                    <th>Objetivo</th>
                                    <th>Parcial</th>
                                    <th>Risco</th>
                                    <th>
                                        PnL 
                                        <i class="ace-icon fa fa-question-circle" data-rel="tooltip" data-placement="left" data-original-title="Previsão de Lucro Líquido: (Lucro - Taxas)"></i>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                              <?php
                              for ($i = 1; $i <= 3; $i++) {
                              ?>
                                <tr>
                                    <td>
                                        <?php
                                            echo $this->Form->input('long-stop'.$i, array('type'=>'text', 'label'=>false,'class'=>'form-control input-mini dolar'));
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $this->Form->input('long-entrada'.$i, array('type'=>'text', 'label'=>false,'class'=>'form-control input-mini dolar'));
                                        ?>
                                    </td>
                                    <td id="long-share<?php echo $i; ?>" valign="center" class="no-hover center">0</td>
                                    <td id="long-objetivo<?php echo $i; ?>" valign="center" class="no-hover center">$0.00</td>
                                    <td id="long-parcial<?php echo $i; ?>" valign="center" class="no-hover center">0</td>
                                    <td id="long-risco<?php echo $i; ?>" valign="center" class="center">$0.00</td>
                                    <td id="long-pnl<?php echo $i; ?>" valign="center" class="center">$0.00</td>
                                </tr>
                              <?php
                              }
                              ?>

                             </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.span -->
        
    <?php
        echo $this->Form->end();
        echo $this->element('rodape_tabela', array('threaded'=>true));
    ?>
        
<script type="text/javascript">
    
    $(".dolar").maskMoney({allowNegative: false, thousands:'', decimal:'.'});

    function long(numero) {
        var entrada = parseFloat($('#CalculadoraLong-entrada'+numero).val());
        var stop    = parseFloat($('#CalculadoraLong-stop'+numero).val());
        var saldo   = parseFloat($('#CalculadoraSaldo').val());
        var exposi  = parseFloat($('#CalculadoraExposicao').val());
        var bp      = parseFloat($('#CalculadoraBp').val());
        var comis   = parseFloat($('#CalculadoraComissao').val());
        var minimo  = parseFloat($('#CalculadoraCmMinima').val());
        var taxa    = parseFloat($('#CalculadoraTaxa').val());
        var risco   = 0; var objetivo = 0; var share = 0; var parcial = 0; var necessario = 0; var pnl = 0; var custoC = 0; var custoT = 0;
        
        if(stop > 0 && entrada > 0) {
            risco      = Number((entrada - stop).toFixed(2));
            objetivo   = Number((entrada + risco).toFixed(2));
            share      = Number(( (saldo*exposi) / risco ).toFixed(0));
            parcial    = Number(( share * 0.7 ).toFixed(0));
            necessario = Number(( entrada * share ).toFixed(2));
            
            pnl        = Number(( risco * share ).toFixed(2));
            custoT     = Number(( share * taxa ).toFixed(3));
            custoC     = Number(( share * comis ).toFixed(3));
            if(custoC < minimo) { custoC = minimo; }
            custoC     = Number(( (custoT + custoC) * 2 ).toFixed(3));
            pnl        = Number(( pnl - custoC ).toFixed(2));
            
            $('#long-risco'+numero).html('$'+risco);
            $('#long-objetivo'+numero).html('$'+objetivo);
            $('#long-share'+numero).html(share);
            $('#long-parcial'+numero).html(parcial);
            $('#long-pnl'+numero).html('$'+pnl);
            
            if(necessario >= bp) {
                $('#long-share'+numero).removeClass('btn-info');
                $('#long-share'+numero).addClass('btn-warning');
            } else {
                $('#long-share'+numero).removeClass('btn-warning');
                $('#long-share'+numero).addClass('btn-info');
            }
            $('#long-objetivo'+numero).addClass('btn-info');
        }
        
        if(stop <= 0 || entrada <= 0) {            
            $('#long-risco'+numero).html('$0.00');
            $('#long-objetivo'+numero).html('$0.00');
            $('#long-share'+numero).html('0');
            $('#long-parcial'+numero).html('0');
            $('#long-pnl'+numero).html('$0.00');
            
            $('#long-share'+numero).removeClass('btn-warning');
            $('#long-share'+numero).removeClass('btn-info');

            $('#long-objetivo'+numero).removeClass('btn-info');
        }
    }
    
    function short(numero) {
        var entrada = parseFloat($('#CalculadoraShort-entrada'+numero).val());
        var stop    = parseFloat($('#CalculadoraShort-stop'+numero).val());
        var saldo   = parseFloat($('#CalculadoraSaldo').val());
        var exposi  = parseFloat($('#CalculadoraExposicao').val());
        var bp      = parseFloat($('#CalculadoraBp').val());
        var comis   = parseFloat($('#CalculadoraComissao').val());
        var minimo  = parseFloat($('#CalculadoraCmMinima').val());
        var taxa    = parseFloat($('#CalculadoraTaxa').val());
        var risco   = 0; var objetivo = 0; var share = 0; var parcial = 0; var necessario = 0; var pnl = 0; var custoC = 0; var custoT = 0;
        
        if(stop > 0 && entrada > 0) {
            risco      = Number((stop - entrada).toFixed(2));
            objetivo   = Number((entrada - risco).toFixed(2));
            share      = Number(( (saldo*exposi) / risco ).toFixed(0));
            parcial    = Number(( share * 0.7 ).toFixed(0));
            necessario = Number(( entrada * share ).toFixed(2));
            
            pnl        = Number(( risco * share ).toFixed(2));
            custoT     = Number(( share * taxa ).toFixed(3));
            custoC     = Number(( share * comis ).toFixed(3));
            if(custoC < minimo) { custoC = minimo; }
            custoC     = Number(( (custoT + custoC) * 2 ).toFixed(3));
            pnl        = Number(( pnl - custoC ).toFixed(2));
            
            $('#short-risco'+numero).html('$'+risco);
            $('#short-objetivo'+numero).html('$'+objetivo);
            $('#short-share'+numero).html(share);
            $('#short-parcial'+numero).html(parcial);
            $('#short-pnl'+numero).html('$'+pnl);
            
            if(necessario >= bp) {
                $('#short-share'+numero).removeClass('btn-info');
                $('#short-share'+numero).addClass('btn-warning');
            } else {
                $('#short-share'+numero).removeClass('btn-warning');
                $('#short-share'+numero).addClass('btn-info');
            }
            $('#short-objetivo'+numero).addClass('btn-info');
        }
        
        if(stop <= 0 || entrada <= 0) {            
            $('#short-risco'+numero).html('$0.00');
            $('#short-objetivo'+numero).html('$0.00');
            $('#short-share'+numero).html('0');
            $('#short-parcial'+numero).html('0');
            $('#short-pnl'+numero).html('$0.00');
            
            $('#short-share'+numero).removeClass('btn-warning');
            $('#short-share'+numero).removeClass('btn-info');

            $('#short-objetivo'+numero).removeClass('btn-info');
        }
    }
    
  <?php
  for ($i = 1; $i <= 3; $i++) {
  ?>    
    $('#CalculadoraLong-entrada<?php echo $i; ?>').bind('keyup', function (event) {
        long(<?php echo $i; ?>);
    });
    $('#CalculadoraLong-stop<?php echo $i; ?>').bind('keyup', function (event) {
        long(<?php echo $i; ?>);
    });
    
    $('#CalculadoraShort-entrada<?php echo $i; ?>').bind('keyup', function (event) {
        short(<?php echo $i; ?>);
    });
    $('#CalculadoraShort-stop<?php echo $i; ?>').bind('keyup', function (event) {
        short(<?php echo $i; ?>);
    });
  <?php
  }
  ?>
    
</script>
        
    </div><!-- /.row -->