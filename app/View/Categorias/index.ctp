<br>
<div class="row">
        <div class="col-xs-12">
            <?php
                $colunas    = array( 
                    array(
                        'nome' => false,
                        'dominio' => 'Categoria',
                        'coluna' => 'usuario_id',
                        'tipo' => 'hidden',
                        'data' => AuthComponent::user('id')
                    ),
                    array(
                        'nome' => 'Nome',
                        'dominio' => 'Categoria',
                        'coluna' => 'nome',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Descrição',
                        'dominio' => 'Categoria',
                        'coluna' => 'descricao',
                        'tipo' => 'text'
                    )
                );
                echo $this->Tabela->imprimir($colunas, $categorias, 'Categoria', array('validationErrors'=>$this->validationErrors, 'threaded'=>true) );
            ?>
        </div>
</div>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>