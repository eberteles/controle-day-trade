<?php
    echo $this->Html->script( 'highcharts/highcharts' );
    echo $this->Html->script( 'highcharts/modules/exporting' );
?>

<div class="row">
    <div class="col-sm-12">
            <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-signal"></i>
                                    Evolução Patrimonial
                            </h4>

                            <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                            <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                            </div>
                    </div>

                    <div class="widget-body">
                        <div id="evolucao" style="min-width: 310px; height: 220px; margin: 0 auto"></div>
                    </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->
    </div><!-- /.col -->
    
    <div class="col-sm-6">
            <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-bar-chart-o"></i>
                                    Média Gain X Loss
                            </h4>

                            <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                            <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                            </div>
                    </div>

                    <div class="widget-body">
                            <div class="widget-main padding-4">
                                    <div id="semana" style="min-width: 310px; height: 220px; margin: 0 auto"></div>
                            </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->
    </div><!-- /.col -->
    
    <div class="col-sm-3">
            <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-star orange"></i>
                                    Média Gain
                            </h4>

                            <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                            <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                            </div>
                    </div>

                    <div class="widget-body">
                            <div class="widget-main no-padding">
                                    <table class="table table-bordered table-striped">
                                            <tbody>
                                                <?php foreach ($top_gain as $stock) { ?>
                                                    <tr>
                                                            <td><?php echo $stock['Acao']['sigla']; ?></td>
                                                            
                                                            <td class="align-right green" data-rel="tooltip" data-placement="left" data-original-title="% de Acerto na Ação."><?php echo number_format( $stock[0]['acerto'] * 100 / ($stock[0]['acerto']+$stock[0]['erro']) , 0, '.', ''); ?>%</td>

                                                            <td class="align-right">
                                                                <b class="blue" data-rel="tooltip" data-placement="left" data-original-title="Média de ganho na Ação."><?php echo $this->Formatacao->moeda($stock[0]['soma']); ?></b>
                                                            </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                    </table>
                            </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->
    </div><!-- /.col -->
    <div class="col-sm-3">
            <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-thumbs-o-down red"></i>
                                    Média Loss
                            </h4>

                            <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                            <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                            </div>
                    </div>

                    <div class="widget-body">
                            <div class="widget-main no-padding">
                                    <table class="table table-bordered table-striped">
                                            <tbody>
                                                <?php foreach ($top_loss as $stock) { ?>
                                                    <tr>
                                                            <td><?php echo $stock['Acao']['sigla']; ?></td>
                                                            
                                                            <td class="align-right red2" data-rel="tooltip" data-placement="left" data-original-title="% de Erro na Ação."><?php echo number_format( $stock[0]['acerto'] * 100 / ($stock[0]['acerto']+$stock[0]['erro']) , 0, '.', ''); ?>%</td>

                                                            <td class="align-right">
                                                                <b class="red" data-rel="tooltip" data-placement="left" data-original-title="Média de perda na Ação."><?php echo $this->Formatacao->moeda($stock[0]['soma']); ?></b>
                                                            </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                    </table>
                            </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->
    </div><!-- /.col -->
    
    <div class="col-sm-12">
            <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-hourglass-start"></i>
                                    Performance Manhã
                            </h4>

                            <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                            <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                            </div>
                    </div>

                    <div class="widget-body">
                            <div class="widget-main padding-3">
                                <div class="col-sm-4">
                                    <div id="tipo_manha" style="min-width: 310px; height: 280px; margin: 0 auto"></div>
                                </div>
                                <div class="col-sm-4">
                                    <div id="setup_manha" style="min-width: 310px; height: 280px; margin: 0 auto"></div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="widget-header widget-header-flat">
                                            <h4 class="widget-title lighter">
                                                    <i class="ace-icon fa fa-tasks green"></i>
                                                    Tempo Médio por Estratégia
                                            </h4>
                                    </div>

                                    <div class="widget-body">
                                            <div class="widget-main no-padding">
                                                    <table class="table table-bordered table-striped">
                                                            <tbody>
                                                                <?php 
                                                                    $colors = array('red-chart', 'orange-chart', 'green-chart', 'blue-chart', 'purple-chart', 'brown-chart');
                                                                    foreach ($setup_manha as $key => $setup) { ?>
                                                                    <tr>
                                                                            <td><i class="ace-icon fa fa-circle <?php echo $colors[$key];?>"></i> <?php echo $setup['Categoria']['nome']; ?></td>

                                                                            <td class="align-right green" data-rel="tooltip" data-placement="left" data-original-title="% de Acerto na Estratégia."><?php echo number_format( $setup[0]['Percentual'] , 1, '.', ''); ?>%</td>

                                                                            <td class="align-right">
                                                                                <b class="blue" data-rel="tooltip" data-placement="left" data-original-title="Tempo Médio na Estratégia."><?php echo $setup[0]['Tempo']; ?></b>
                                                                            </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                    </table>
                                            </div>
                                    </div>
                                </div>
                            </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->
    </div><!-- /.col -->
    
    <div class="col-sm-12">
            <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-hourglass-end"></i>
                                    Performance Tarde
                            </h4>

                            <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                            <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                            </div>
                    </div>

                    <div class="widget-body">
                            <div class="widget-main padding-3">
                                <div class="col-sm-4">
                                    <div id="tipo_tarde" style="min-width: 310px; height: 280px; margin: 0 auto"></div>
                                </div>
                                <div class="col-sm-4">
                                    <div id="setup_tarde" style="min-width: 310px; height: 280px; margin: 0 auto"></div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="widget-header widget-header-flat">
                                            <h4 class="widget-title lighter">
                                                    <i class="ace-icon fa fa-tasks green"></i>
                                                    Tempo Médio por Estratégia
                                            </h4>
                                    </div>

                                    <div class="widget-body">
                                            <div class="widget-main no-padding">
                                                    <table class="table table-bordered table-striped">
                                                            <tbody>
                                                                <?php 
                                                                    $colors = array('brown-chart', 'purple-chart', 'blue-chart', 'green-chart', 'orange-chart', 'red-chart');
                                                                    foreach ($setup_tarde as $key => $setup) { ?>
                                                                    <tr>
                                                                            <td><i class="ace-icon fa fa-circle <?php echo $colors[$key];?>"></i> <?php echo $setup['Categoria']['nome']; ?></td>

                                                                            <td class="align-right green" data-rel="tooltip" data-placement="left" data-original-title="% de Acerto na Estratégia."><?php echo number_format( $setup[0]['Percentual'] , 0, '.', ''); ?>%</td>

                                                                            <td class="align-right">
                                                                                <b class="blue" data-rel="tooltip" data-placement="left" data-original-title="Tempo Médio na Estratégia."><?php echo $setup[0]['Tempo']; ?></b>
                                                                            </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                    </table>
                                            </div>
                                    </div>
                                </div>
                            </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->
    </div><!-- /.col -->
</div>

<script type="text/javascript">
      
Highcharts.setOptions({
    lang: {
        months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        loading: ['Atualizando o gráfico...aguarde'],
        contextButtonTitle: 'Exportar gráfico',
        decimalPoint: ',',
        thousandsSep: '.',
        downloadJPEG: 'Baixar imagem JPEG',
        downloadPDF: 'Baixar arquivo PDF',
        downloadPNG: 'Baixar imagem PNG',
        downloadSVG: 'Baixar vetor SVG',
        printChart: 'Imprimir gráfico',
        rangeSelectorFrom: 'De',
        rangeSelectorTo: 'Para',
        rangeSelectorZoom: 'Zoom',
        resetZoom: 'Limpar Zoom',
        resetZoomTitle: 'Voltar Zoom para nível 1:1',
    }
});
      
        
    var saldos = <?php echo $saldos; ?>

    Highcharts.chart('evolucao', {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: false
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'area',
            name: 'Saldo',
            data: saldos
        }]
    });
    
    Highcharts.chart('semana', {
        title: {
            text: false
        },
        colors: ['#90ee7e', '#6ab14a', '#f7a35c', '#f59e00'],
        //colors: ['green', 'red'],
        subtitle: {
            text: false
        },
        xAxis: {
            categories: ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: false,
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                }
            }
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            floating: false,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Gain (Long)',
            data: <?php echo $gain_semana_long; ?>
        }, {
            name: 'Gain (Short)',
            data: <?php echo $gain_semana_short; ?>
        }, {
            name: 'Loss (Long)',
            data: <?php echo $loss_semana_long; ?>
        }, {
            name: 'Loss (Short)',
            data: <?php echo $loss_semana_short; ?>
        }]
    });
    
    Highcharts.chart('setup_manha', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Acerto por Estratégia'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        colors: ['red', 'orange', 'green', 'blue', 'purple', 'brown'],
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Percentual',
            data: [
            <?php 
            foreach ($setup_manha as $key => $setup) {
                echo "{ name: '" . $setup['Categoria']['nome'] . "', y: " . $setup[0]['Percentual'] . ( ($key == 0) ? ", sliced: true, selected: true " : "" ) . " },";
            } ?>
            ]
        }]
    });
    
    Highcharts.chart('tipo_manha', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: '% Acerto',
//            align: 'center',
//            verticalAlign: 'middle',
//            y: 40
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '50%']
            }
        },
        colors: ['#90ee7e', '#f7a35c'],
        credits: {
            enabled: false
        },
        series: [{
            type: 'pie',
            name: '% Acerto',
            innerSize: '50%',
            data: [
            <?php 
            foreach ($tipo_manha as $tipo) {
                echo "{ name: '" . ( ($tipo['Posicao']['tipo']=='S')? "Short": "Long" ) . "', y: " . $tipo[0]['Percentual'] . " },";
            } ?>
            ]
        }]
    });
    
    Highcharts.chart('setup_tarde', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Acerto por Estratégia'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        colors: ['brown', 'purple', 'blue', 'green', 'orange', 'red'],
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Percentual',
            data: [
            <?php 
            foreach ($setup_tarde as $key => $setup) {
                echo "{ name: '" . $setup['Categoria']['nome'] . "', y: " . $setup[0]['Percentual'] . ( ($key == 0) ? ", sliced: true, selected: true " : "" ) . " },";
            } ?>
            ]
        }]
    });
    
    Highcharts.chart('tipo_tarde', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: '% Acerto',
//            align: 'center',
//            verticalAlign: 'middle',
//            y: 40
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '50%']
            }
        },
        colors: ['#90ee7e', '#f7a35c'],
        credits: {
            enabled: false
        },
        series: [{
            type: 'pie',
            name: '% Acerto',
            innerSize: '50%',
            data: [
            <?php 
            foreach ($tipo_tarde as $tipo) {
                echo "{ name: '" . ( ($tipo['Posicao']['tipo']=='S')? "Short": "Long" ) . "', y: " . $tipo[0]['Percentual'] . " },";
            } ?>
            ]
        }]
    });
   
</script>