<?php
    echo $this->Html->script( 'highcharts/highcharts' );
    echo $this->Html->script( 'highcharts/modules/drilldown' );
?>

<div class="row">
    <?php echo $this->Form->create('Posicao'); ?>
    <div class="col-xs-2">
    <?php
        echo $this->Form->input('data', array('class'=>'form-control date-picker', 'required'=>'required', 'label'=>false, 'value'=>$data));
    ?>
    </div>
    <div class="col-xs-2">
        <button type="submit" class="btn btn-sm btn-primary"><i class="ace-icon fa fa-check"></i>Pesquisar</button>
    </div>
    <div class="col-xs-4">
        <h5><b><?php echo $this->Formatacao->getDiaSemana($data); ?></b></h5>
    </div>
    <?php
        echo $this->Form->end();
    ?>    
</div>

<?php
    echo $this->Html->script( 'highcharts/highcharts' );
    echo $this->Html->script( 'highcharts/modules/drilldown' );
    
    if(count($posicoes) > 0) {
        
        $dia    = array('nuOperacoes'=>count($posicoes), 'percentual'=>0, 'pnl'=>0, 'qtd_gain'=>0, 'qtd_loss'=>0, 'valores'=>'', 'short'=>0, 'long'=>0, 'setup_short'=>array(), 'setup_long'=>array());
        $manha  = array('nuOperacoes'=>0, 'percentual'=>0, 'pnl'=>0, 'qtd_gain'=>0, 'qtd_loss'=>0, 'valores'=>'', 'short'=>0, 'long'=>0, 'setup_short'=>array(), 'setup_long'=>array());
        $tarde  = array('nuOperacoes'=>0, 'percentual'=>0, 'pnl'=>0, 'qtd_gain'=>0, 'qtd_loss'=>0, 'valores'=>'', 'short'=>0, 'long'=>0, 'setup_short'=>array(), 'setup_long'=>array());
    
        $categorias     = array();
        
        foreach ($posicoes as $key => $posicao) {
            $dia['percentual'] += $posicao['Posicao']['crescimento'];
            $dia['pnl']        += $posicao['Posicao']['liquido'];
            ($posicao['Posicao']['liquido'] > 0) ? $dia['qtd_gain']++ : $dia['qtd_loss']++;
            $dia['valores']    .= (($dia['valores'] != '') ? ',' : '') . '[' . $this->Formatacao->getDataJs($posicao['Posicao']['created']) . ',' . number_format( $posicao['Posicao']['liquido'], 2, '.', ',') . ']';
            if( $posicao['Posicao']['tipo'] == 'S' ) {
                $dia['short']++;
                ( isset($dia['setup_short'][$posicao['Posicao']['categoria_id']]) ) ? $dia['setup_short'][$posicao['Posicao']['categoria_id']]++ : $dia['setup_short'][$posicao['Posicao']['categoria_id']] = 1 ;
            } else {
                $dia['long']++;
                ( isset($dia['setup_long'][$posicao['Posicao']['categoria_id']]) ) ? $dia['setup_long'][$posicao['Posicao']['categoria_id']]++ : $dia['setup_long'][$posicao['Posicao']['categoria_id']] = 1 ;
            }
            
            if($posicao['Posicao']['periodo'] == 'M') {
                $manha['percentual'] += $posicao['Posicao']['crescimento'];
                $manha['pnl']        += $posicao['Posicao']['liquido'];
                ($posicao['Posicao']['liquido'] > 0) ? $manha['qtd_gain']++ : $manha['qtd_loss']++;
                $manha['valores']    .= (($manha['valores'] != '') ? ',' : '') . '[' . $this->Formatacao->getDataJs($posicao['Posicao']['created']) . ',' . number_format( $posicao['Posicao']['liquido'], 2, '.', ',') . ']';
                //$manha['valores']    .= ($manha['nuOperacoes'] < 8) ? ( (($manha['valores'] != '') ? ',' : '') . number_format( $posicao['Posicao']['liquido'], 0, ',', '.') ) : '';
                if( $posicao['Posicao']['tipo'] == 'S' ) {
                    $manha['short']++;
                    ( isset($manha['setup_short'][$posicao['Posicao']['categoria_id']]) ) ? $manha['setup_short'][$posicao['Posicao']['categoria_id']]++ : $manha['setup_short'][$posicao['Posicao']['categoria_id']] = 1 ;
                } else {
                    $manha['long']++;
                    ( isset($manha['setup_long'][$posicao['Posicao']['categoria_id']]) ) ? $manha['setup_long'][$posicao['Posicao']['categoria_id']]++ : $manha['setup_long'][$posicao['Posicao']['categoria_id']] = 1 ;
                }
                $manha['nuOperacoes']++;
            } else {
                $tarde['percentual'] += $posicao['Posicao']['crescimento'];
                $tarde['pnl']        += $posicao['Posicao']['liquido'];
                ($posicao['Posicao']['liquido'] > 0) ? $tarde['qtd_gain']++ : $tarde['qtd_loss']++;
                $tarde['valores']    .= (($tarde['valores'] != '') ? ',' : '') . '[' . $this->Formatacao->getDataJs($posicao['Posicao']['created']) . ',' . number_format( $posicao['Posicao']['liquido'], 2, '.', ',') . ']';
                //$tarde['valores']    .= ($tarde['nuOperacoes'] < 8) ? ( (($tarde['valores'] != '') ? ',' : '') . number_format( $posicao['Posicao']['liquido'], 0, ',', '.') ) : '';
                if( $posicao['Posicao']['tipo'] == 'S' ) {
                    $tarde['short']++;
                    ( isset($tarde['setup_short'][$posicao['Posicao']['categoria_id']]) ) ? $tarde['setup_short'][$posicao['Posicao']['categoria_id']]++ : $tarde['setup_short'][$posicao['Posicao']['categoria_id']] = 1 ;
                } else {
                    $tarde['long']++;
                    ( isset($tarde['setup_long'][$posicao['Posicao']['categoria_id']]) ) ? $tarde['setup_long'][$posicao['Posicao']['categoria_id']]++ : $tarde['setup_long'][$posicao['Posicao']['categoria_id']] = 1 ;
                }
                $tarde['nuOperacoes']++;
            }            
            
            $categorias[$posicao['Posicao']['categoria_id']]    = $posicao['Categoria']['nome'];
        }
        $dia['taxa_acerto'] = (100 * $dia['qtd_gain']) / $dia['nuOperacoes'];
        $dia['pct_short']   = (100 * $dia['short']) / $dia['nuOperacoes'];
        $dia['pct_long']    = (100 * $dia['long']) / $dia['nuOperacoes'];
        
        $manha['taxa_acerto'] = ($manha['nuOperacoes'] > 0) ? (100 * $manha['qtd_gain']) / $manha['nuOperacoes'] : 0;
        $manha['pct_short']   = ($manha['nuOperacoes'] > 0) ? (100 * $manha['short']) / $manha['nuOperacoes'] : 0;
        $manha['pct_long']    = ($manha['nuOperacoes'] > 0) ? (100 * $manha['long']) / $manha['nuOperacoes'] : 0;

        $tarde['taxa_acerto'] = ($tarde['nuOperacoes'] > 0) ? (100 * $tarde['qtd_gain']) / $tarde['nuOperacoes'] : 0;
        $tarde['pct_short']   = ($tarde['nuOperacoes'] > 0) ?(100 * $tarde['short']) / $tarde['nuOperacoes'] : 0;
        $tarde['pct_long']    = ($tarde['nuOperacoes'] > 0) ?(100 * $tarde['long']) / $tarde['nuOperacoes'] : 0;
    
?>

<div class="row">
    
    <div class="col-sm-12 widget-container-col">
            <div class="widget-box">
                    <div class="widget-header widget-header-small">
                            <h5 class="widget-title smaller">
                                    <i class="ace-icon fa fa-signal"></i>
                                    Indicadores
                            </h5>

                            <!-- #section:custom/widget-box.tabbed -->
                            <div class="widget-toolbar no-border">
                                    <ul class="nav nav-tabs" id="myTab">
                                            <li class="active">
                                                    <a data-toggle="tab" href="#home">Dia</a>
                                            </li>

                                            <li>
                                                    <a data-toggle="tab" href="#profile">Manhã</a>
                                            </li>

                                            <li>
                                                    <a data-toggle="tab" href="#info">Tarde</a>
                                            </li>
                                    </ul>
                            </div>

                            <!-- /section:custom/widget-box.tabbed -->
                    </div>

                    <div class="widget-body">
                        <div class="widget-main padding-6">
                            <div class="tab-content">
                                <div id="home" class="tab-pane in active">
                                    <?php echo $this->element('dia', array('consolidado'=>$dia, 'grafico'=>'dia', 'categorias'=>$categorias, 'titulo_acerto'=>'acerto operação')); ?>
                                </div>

                                <div id="profile" class="tab-pane">
                                    <?php echo $this->element('dia', array('consolidado'=>$manha, 'grafico'=>'manha', 'categorias'=>$categorias, 'titulo_acerto'=>'acerto operação')); ?>
                                </div>

                                <div id="info" class="tab-pane">
                                    <?php echo $this->element('dia', array('consolidado'=>$tarde, 'grafico'=>'tarde', 'categorias'=>$categorias, 'titulo_acerto'=>'acerto operação')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
    <div class="col-sm-12">
    
        <table class="tree table table-striped table-bordered table-hover">
        <thead>
            <tr><th>Ação</th><th>Setup</th><th>Tipo</th><th>P/L</th><th>Comissão</th><th>Taxa</th><th><b>Líquido</b></th><th>%</th></tr></thead>
        <tbody>
        <?php
            $total  = 0; $totalBruto  = 0; $totalComissao  = 0; $totalTaxa  = 0; $totalCrescimento= 0;
            foreach ($posicoes as $key => $posicao) {
                $total         += ($posicao['Posicao']['total'] - $posicao['Posicao']['comissao'] - $posicao['Posicao']['taxa']);
                $totalBruto    += $posicao['Posicao']['total'];
                $totalComissao += $posicao['Posicao']['comissao'];
                $totalTaxa     += $posicao['Posicao']['taxa'];                
                $totalCrescimento += $posicao['Posicao']['crescimento'];                
        ?>
            <tr class="treegrid-<?php echo $key; ?>" style="background-color: <?php echo ($posicao['Posicao']['resultado']=='P')?'#ccffcc':'#ffcccc'; ?>">
                    <td><?php echo $posicao['Acao']['sigla']; ?></td>
                    <td><?php echo $posicao['Categoria']['nome']; ?></td>
                    <td><?php echo ($posicao['Posicao']['tipo']=='S')?'Short':'Long'; ?></td>
                    <td><?php echo $this->Formatacao->moeda($posicao['Posicao']['total']); ?></td>
                    <td><?php echo ($posicao['Posicao']['comissao'] == 0)?'-':$this->Formatacao->moeda($posicao['Posicao']['comissao']); ?></td>
                    <td><?php echo ($posicao['Posicao']['taxa'] == 0)?'-':$this->Formatacao->moeda($posicao['Posicao']['taxa']); ?></td>
                    <td><b><?php echo $this->Formatacao->moeda($posicao['Posicao']['total'] - $posicao['Posicao']['comissao'] - $posicao['Posicao']['taxa']); ?></b></td>
                    <td><?php echo $posicao['Posicao']['crescimento']; ?></td>
            </tr>
            <tr class="treegrid-ticket-<?php echo $key; ?> treegrid-parent-<?php echo $key; ?>">
                <td colspan="7">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr><td>Hora</td><td>Shares</td><td>Preço</td><td>Tipo</td></tr></thead>
                        <tbody>
                        <?php
                            foreach ($posicao['Ticket'] as $keyTicket => $ticket) {
                                $color = ($ticket['tipo']=='S')?'#990000':'#003333';
                        ?>
                          <tr>
                            <td style="color: <?php echo $color; ?>"><?php echo $ticket['time']; ?></td>
                            <td style="color: <?php echo $color; ?>"><?php echo $ticket['share']; ?></td>
                            <td style="color: <?php echo $color; ?>"><?php echo $this->Formatacao->moeda($ticket['preco']); ?></td>
                            <td style="color: <?php echo $color; ?>"><?php echo ($ticket['tipo']=='S')?'Short':'Buy'; ?></td>
                          </tr>
                        <?php
                            }
                        ?>
                        </tbody>
                    </table>
                </td>
                <td>
                  <?php 
                    echo $posicao['Posicao']['comentario'];
                    //echo $this->Form->input('Entrada.file.'.$key, array('type'=>'file', 'class'=>'upload', 'label'=>false));
                  ?>
                </td>
            </tr>
        <?php
            }
        ?>
        </tbody>
        
        <thead>
            <tr>
                <th colspan="3">Totais</th>
                <th><?php echo $this->Formatacao->moeda($totalBruto); ?></th>
                <th><?php echo $this->Formatacao->moeda($totalComissao); ?></th>
                <th><?php echo $this->Formatacao->moeda($totalTaxa); ?></th>
                <th><?php echo $this->Formatacao->moeda($total); ?></th>
                <th><?php echo $totalCrescimento; ?></th>
            </tr>
        </thead>
    </table>
    </div>
    
</div>

<?php
    } else {
        echo '<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                    </button>
                    Não foram realizadas operações neste dia.
                    </font></font><br>
            </div>';
    }
?>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>


<script type="text/javascript">
    
    $('.tree').treegrid({
        initialState: 'collapsed',
        expanderExpandedClass: 'fa fa-minus-circle',
        expanderCollapsedClass: 'fa fa-plus-circle'
    });

    $('.date-picker').datepicker({
        mask:true,
        format:'dd/mm/yyyy'
    });
    
//    $('.nav-tabs a').on('shown.bs.tab', function(){
//        $.sparkline_display_visible();
//    });
    
</script>