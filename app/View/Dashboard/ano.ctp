<?php
    echo $this->Html->script( 'highcharts/highcharts' );
    echo $this->Html->script( 'highcharts/modules/drilldown' );
?>

<div class="row">
    <?php echo $this->Form->create('Posicao'); ?>
    <div class="col-xs-2">
    <?php
        echo $this->Form->input('data', array('class'=>'form-control ano', 'required'=>'required', 'label'=>false, 'value'=>$data));
    ?>
    </div>
    <div class="col-xs-2">
        <button type="submit" class="btn btn-sm btn-primary"><i class="ace-icon fa fa-check"></i>Pesquisar</button>
    </div>
    <div class="col-xs-4">
        <h5><b><?php echo $data; ?></b></h5>
    </div>
    <?php
        echo $this->Form->end();
    ?>    
</div>

<?php
    echo $this->Html->script( 'highcharts/highcharts' );
    echo $this->Html->script( 'highcharts/modules/drilldown' );
    
    if(count($posicoes) > 0) {
        
        $dia    = array('nuOperacoes'=>count($posicoes), 'percentual'=>0, 'pnl'=>0, 'acerto'=>0, 'valores'=>'', 'qtd_gain'=>0, 'qtd_loss'=>0, 'short'=>0, 'long'=>0, 'setup_short'=>array(), 'setup_long'=>array());
        $manha  = array('nuOperacoes'=>0, 'percentual'=>0, 'pnl'=>0, 'acerto'=>0, 'valores'=>'', 'qtd_gain'=>0, 'qtd_loss'=>0, 'short'=>0, 'long'=>0, 'setup_short'=>array(), 'setup_long'=>array());
        $tarde  = array('nuOperacoes'=>0, 'percentual'=>0, 'pnl'=>0, 'acerto'=>0, 'valores'=>'', 'qtd_gain'=>0, 'qtd_loss'=>0, 'short'=>0, 'long'=>0, 'setup_short'=>array(), 'setup_long'=>array());
    
        $categorias     = array();
        
        foreach ($posicoesDia as $key => $posicao) {
            $dia['qtd_gain']   += ($posicao[0]['valor'] >= 0) ? 1 : 0;
            $dia['qtd_loss']   += ($posicao[0]['valor'] < 0) ? 1 : 0;
            $dia['valores']    .= (($dia['valores'] != '') ? ',' : '') . '[' . $this->Formatacao->getDataJs($posicao['Posicao']['created']) . ',' . number_format( $posicao[0]['valor'], 2, '.', ',') . ']';
        }
        foreach ($posicoesManha as $key => $posicao) {
            $manha['qtd_gain']   += ($posicao[0]['valor'] >= 0) ? 1 : 0;
            $manha['qtd_loss']   += ($posicao[0]['valor'] < 0) ? 1 : 0;
            $manha['valores']    .= (($manha['valores'] != '') ? ',' : '') . '[' . $this->Formatacao->getDataJs($posicao['Posicao']['created']) . ',' . number_format( $posicao[0]['valor'], 2, '.', ',') . ']';
        }
        foreach ($posicoesTarde as $key => $posicao) {
            $tarde['qtd_gain']   += ($posicao[0]['valor'] >= 0) ? 1 : 0;
            $tarde['qtd_loss']   += ($posicao[0]['valor'] < 0) ? 1 : 0;
            $tarde['valores']    .= (($tarde['valores'] != '') ? ',' : '') . '[' . $this->Formatacao->getDataJs($posicao['Posicao']['created']) . ',' . number_format( $posicao[0]['valor'], 2, '.', ',') . ']';
        }
        
        foreach ($posicoes as $key => $posicao) {
            $dia['percentual'] += $posicao['Posicao']['crescimento'];
            $dia['pnl']        += $posicao['Posicao']['liquido'];
            ($posicao['Posicao']['liquido'] > 0) ? $dia['acerto']++ : false;

            if( $posicao['Posicao']['tipo'] == 'S' ) {
                $dia['short']++;
                ( isset($dia['setup_short'][$posicao['Posicao']['categoria_id']]) ) ? $dia['setup_short'][$posicao['Posicao']['categoria_id']]++ : $dia['setup_short'][$posicao['Posicao']['categoria_id']] = 1 ;
            } else {
                $dia['long']++;
                ( isset($dia['setup_long'][$posicao['Posicao']['categoria_id']]) ) ? $dia['setup_long'][$posicao['Posicao']['categoria_id']]++ : $dia['setup_long'][$posicao['Posicao']['categoria_id']] = 1 ;
            }
            
            if($posicao['Posicao']['periodo'] == 'M') {
                $manha['percentual'] += $posicao['Posicao']['crescimento'];
                $manha['pnl']        += $posicao['Posicao']['liquido'];
                ($posicao['Posicao']['liquido'] > 0) ? $manha['acerto']++ : false;

                if( $posicao['Posicao']['tipo'] == 'S' ) {
                    $manha['short']++;
                    ( isset($manha['setup_short'][$posicao['Posicao']['categoria_id']]) ) ? $manha['setup_short'][$posicao['Posicao']['categoria_id']]++ : $manha['setup_short'][$posicao['Posicao']['categoria_id']] = 1 ;
                } else {
                    $manha['long']++;
                    ( isset($manha['setup_long'][$posicao['Posicao']['categoria_id']]) ) ? $manha['setup_long'][$posicao['Posicao']['categoria_id']]++ : $manha['setup_long'][$posicao['Posicao']['categoria_id']] = 1 ;
                }
                $manha['nuOperacoes']++;
            } else {
                $tarde['percentual'] += $posicao['Posicao']['crescimento'];
                $tarde['pnl']        += $posicao['Posicao']['liquido'];
                ($posicao['Posicao']['liquido'] > 0) ? $tarde['acerto']++ : false;

                if( $posicao['Posicao']['tipo'] == 'S' ) {
                    $tarde['short']++;
                    ( isset($tarde['setup_short'][$posicao['Posicao']['categoria_id']]) ) ? $tarde['setup_short'][$posicao['Posicao']['categoria_id']]++ : $tarde['setup_short'][$posicao['Posicao']['categoria_id']] = 1 ;
                } else {
                    $tarde['long']++;
                    ( isset($tarde['setup_long'][$posicao['Posicao']['categoria_id']]) ) ? $tarde['setup_long'][$posicao['Posicao']['categoria_id']]++ : $tarde['setup_long'][$posicao['Posicao']['categoria_id']] = 1 ;
                }
                $tarde['nuOperacoes']++;
            }            
            
            $categorias[$posicao['Posicao']['categoria_id']]    = $posicao['Categoria']['nome'];
        }
        $dia['taxa_acerto'] = (100 * $dia['qtd_gain']) / count($posicoesDia);
        $dia['pct_short']   = (100 * $dia['short']) / $dia['nuOperacoes'];
        $dia['pct_long']    = (100 * $dia['long']) / $dia['nuOperacoes'];
        
        $manha['taxa_acerto'] = (count($posicoesManha) > 0) ? (100 * $manha['qtd_gain']) / count($posicoesManha) : 0;
        $manha['pct_short']   = ($manha['nuOperacoes'] > 0) ? (100 * $manha['short']) / $manha['nuOperacoes'] : 0;
        $manha['pct_long']    = ($manha['nuOperacoes'] > 0) ? (100 * $manha['long']) / $manha['nuOperacoes'] : 0;

        $tarde['taxa_acerto'] = (count($posicoesTarde) > 0) ? (100 * $tarde['qtd_gain']) / count($posicoesTarde) : 0;
        $tarde['pct_short']   = ($tarde['nuOperacoes'] > 0) ?(100 * $tarde['short']) / $tarde['nuOperacoes'] : 0;
        $tarde['pct_long']    = ($tarde['nuOperacoes'] > 0) ?(100 * $tarde['long']) / $tarde['nuOperacoes'] : 0;
    
?>

<div class="row">
    
    <div class="col-sm-12 widget-container-col">
            <div class="widget-box">
                    <div class="widget-header widget-header-small">
                            <h5 class="widget-title smaller">
                                    <i class="ace-icon fa fa-signal"></i>
                                    Indicadores
                            </h5>

                            <!-- #section:custom/widget-box.tabbed -->
                            <div class="widget-toolbar no-border">
                                    <ul class="nav nav-tabs" id="myTab">
                                            <li class="active">
                                                    <a data-toggle="tab" href="#home">Dia</a>
                                            </li>

                                            <li>
                                                    <a data-toggle="tab" href="#profile">Manhã</a>
                                            </li>

                                            <li>
                                                    <a data-toggle="tab" href="#info">Tarde</a>
                                            </li>
                                    </ul>
                            </div>

                            <!-- /section:custom/widget-box.tabbed -->
                    </div>

                    <div class="widget-body">
                        <div class="widget-main padding-6">
                            <div class="tab-content">
                                <div id="home" class="tab-pane in active">
                                    <?php echo $this->element('dia', array('consolidado'=>$dia, 'grafico'=>'dia', 'categorias'=>$categorias, 'titulo_acerto'=>'acerto diário')); ?>
                                </div>

                                <div id="profile" class="tab-pane">
                                    <?php echo $this->element('dia', array('consolidado'=>$manha, 'grafico'=>'manha', 'categorias'=>$categorias, 'titulo_acerto'=>'acerto diário')); ?>
                                </div>

                                <div id="info" class="tab-pane">
                                    <?php echo $this->element('dia', array('consolidado'=>$tarde, 'grafico'=>'tarde', 'categorias'=>$categorias, 'titulo_acerto'=>'acerto diário')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </div>

    <div class="col-sm-12">
        <br>
    <table class="tree table table-striped table-bordered table-hover">
        <thead>
            <tr><th>Mês</th><th>Operações</th><th>P/L</th><th>Comissão</th><th>Taxa</th><th><b>Líquido</b></th><th>%</th></tr>
        </thead>
        
        <tbody>
        <?php
            $total  = 0; $totalBruto  = 0; $totalComissao  = 0; $totalTaxa  = 0; $totalOperacao = 0; $totalCrescimento = 0;
            foreach ($posicoesAno as $key => $posicao) {
                $total         += $posicao[0]['liquido'];
                $totalBruto    += $posicao[0]['total'];
                $totalComissao += $posicao[0]['comissao'];
                $totalTaxa     += $posicao[0]['taxa'];
                $totalOperacao += $posicao[0]['operacoes'];
                $totalCrescimento += $posicao[0]['crescimento'];
        ?>
            <tr style="background-color: <?php echo ($posicao[0]['crescimento'] > 0)?'#ccffcc':'#ffcccc'; ?>">
                    <td><?php 
                        echo $this->Html->link(
                            $this->Formatacao->meses[$posicao[0]['mes']-1],
                            array(
                                'controller' => 'dashboard',
                                'action' => 'mes',
                                $posicao[0]['mes'], $data
                            )
                        ); ?>
                    </td>
                    <td><?php echo $posicao[0]['operacoes']; ?></td>
                    <td><?php echo $this->Formatacao->moeda($posicao[0]['total']); ?></td>
                    <td><?php echo ($posicao[0]['comissao'] == 0)?'-':$this->Formatacao->moeda($posicao[0]['comissao']); ?></td>
                    <td><?php echo ($posicao[0]['taxa'] == 0)?'-':$this->Formatacao->moeda($posicao[0]['taxa']); ?></td>
                    <td><b><?php echo $this->Formatacao->moeda($posicao[0]['liquido']); ?></b></td>
                    <td><?php echo $posicao[0]['crescimento']; ?></td>
            </tr>
        <?php
            }
        ?>
        </tbody>
        
        <thead>
            <tr>
                <th>Totais</th>
                <th><?php echo $totalOperacao; ?></th>
                <th><?php echo $this->Formatacao->moeda($totalBruto); ?></th>
                <th><?php echo $this->Formatacao->moeda($totalComissao); ?></th>
                <th><?php echo $this->Formatacao->moeda($totalTaxa); ?></th>
                <th><?php echo $this->Formatacao->moeda($total); ?></th>
                <th><?php echo $totalCrescimento; ?></th>
            </tr>
        </thead>
        
    </table>
    </div>
    
</div>

<?php
    } else {
        echo '<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                    </button>
                    Não foram realizadas operações neste ano.
                    </font></font><br>
            </div>';
    }
?>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>


<script type="text/javascript">
    
    $('.tree').treegrid({
        initialState: 'collapsed',
        expanderExpandedClass: 'fa fa-minus-circle',
        expanderCollapsedClass: 'fa fa-plus-circle'
    });

    $('.ano').mask('9999');
    
//    $('.nav-tabs a').on('shown.bs.tab', function(){
//        $.sparkline_display_visible();
//    });
    
</script>