<?php

class DashboardController extends AppController {
    
    public function dia($dia = null, $mes = null, $ano = null) {
        
        $this->loadModel('Posicao');
        
        $conta  = $this->Session->read('conta');
        
        $data = DateTime::createFromFormat('d/m/Y', (isset($this->data['Posicao']['data']) ? $this->data['Posicao']['data'] : ( ($dia != null) ? ($dia . "/" . $mes . "/" . $ano) : date('d/m/Y')) ) );
        
        $posicoes = $this->Posicao->find('all', array(
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'date(created)' => $data->format("Y-m-d") ),
            'order' => array( 'Posicao.created'),
            ) );
        
        $this->set('posicoes', $posicoes );
        $this->set('data', $data->format("d/m/Y") );
    }
    
    public function mes($mes = null, $ano = null) {
        
        $this->loadModel('Posicao');
        
        $conta  = $this->Session->read('conta');
        
        $data = (isset($this->data['Posicao']['data'])) ? DateTime::createFromFormat('m/Y', $this->data['Posicao']['data']) : ( ($mes != null && $ano != null) ? DateTime::createFromFormat('m/Y', $mes . "/" . $ano) : DateTime::createFromFormat('Y-m', date('Y-m')) );
        
        $posicoes = $this->Posicao->find('all', array(
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)' => $data->format("Y"), 'MONTH(created)' => $data->format("m") ),
            'order' => array( 'Posicao.created'),
            ) );
        
        $posicoesDia = $this->Posicao->find('all', array(
            'recursive' => -1,
            'fields' => array( 'SUM(Posicao.liquido) AS valor', 'Posicao.created' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)' => $data->format("Y"), 'MONTH(created)' => $data->format("m") ),
            'order' => array( 'Posicao.created'),
            'group' => array('YEAR(created)', 'MONTH(created)', 'DAY(created)'),
            ) );
        
        $posicoesManha  = $this->Posicao->find('all', array(
            'recursive' => -1,
            'fields' => array( 'SUM(Posicao.liquido) AS valor', 'Posicao.created' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 
                'YEAR(created)' => $data->format("Y"), 'MONTH(created)' => $data->format("m"), 'periodo'=>'M' ),
            'order' => array( 'Posicao.created'),
            'group' => array('YEAR(created)', 'MONTH(created)', 'DAY(created)'),
            ) );
        
        $posicoesTarde  = $this->Posicao->find('all', array(
            'recursive' => -1,
            'fields' => array( 'SUM(Posicao.liquido) AS valor', 'Posicao.created' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 
                'YEAR(created)' => $data->format("Y"), 'MONTH(created)' => $data->format("m"), 'periodo'=>'T' ),
            'order' => array( 'Posicao.created'),
            'group' => array('YEAR(created)', 'MONTH(created)', 'DAY(created)'),
            ) );
        
        $this->set('posicoes', $posicoes );
        $this->set('posicoesDia', $posicoesDia );
        $this->set('posicoesManha', $posicoesManha );
        $this->set('posicoesTarde', $posicoesTarde );
        $this->set('data', $data->format("m/Y") );
    }
    
    public function ano() {
        
        $this->loadModel('Posicao');
        
        $conta  = $this->Session->read('conta');
        
        $data = (isset($this->data['Posicao']['data'])) ? $this->data['Posicao']['data'] : date('Y');
        
        $posicoes = $this->Posicao->find('all', array(
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)' => $data ),
            'order' => array( 'Posicao.created'),
            ) );
        
        $posicoesDia = $this->Posicao->find('all', array(
            'recursive' => -1,
            'fields' => array( 'SUM(Posicao.liquido) AS valor', 'Posicao.created' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)' => $data ),
            'order' => array( 'Posicao.created'),
            'group' => array('YEAR(created)', 'MONTH(created)', 'DAY(created)'),
            ) );
        
        $posicoesAno = $this->Posicao->find('all', array(
            'recursive' => -1,
            'fields' => array('COUNT(id) AS operacoes', 'SUM(Posicao.total) AS total', 'SUM(Posicao.comissao) AS comissao', 'SUM(Posicao.taxa) AS taxa', 
                'SUM(Posicao.liquido) AS liquido', 'SUM(Posicao.crescimento) AS crescimento', 'MONTH(created) AS mes' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)' => $data ),
            'order' => array( 'Posicao.created'),
            'group' => array('MONTH(created)'),
            ) );
        
        $posicoesManha  = $this->Posicao->find('all', array(
            'recursive' => -1,
            'fields' => array( 'SUM(Posicao.liquido) AS valor', 'Posicao.created' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)' => $data, 'periodo'=>'M' ),
            'order' => array( 'Posicao.created'),
            'group' => array('YEAR(created)', 'MONTH(created)', 'DAY(created)'),
            ) );
        
        $posicoesTarde  = $this->Posicao->find('all', array(
            'recursive' => -1,
            'fields' => array( 'SUM(Posicao.liquido) AS valor', 'Posicao.created' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)' => $data, 'periodo'=>'T' ),
            'order' => array( 'Posicao.created'),
            'group' => array('YEAR(created)', 'MONTH(created)', 'DAY(created)'),
            ) );
        
        $this->set('posicoesAno', $posicoesAno );
        $this->set('posicoes', $posicoes );
        $this->set('posicoesDia', $posicoesDia );
        $this->set('posicoesManha', $posicoesManha );
        $this->set('posicoesTarde', $posicoesTarde );
        $this->set('data', $data );
    }
    
    public function index() {
        
        $conta  = $this->Session->read('conta');
        
        if(count($conta) <=0) {
            $this->redirect(array(
                'controller' => 'entradas',
                'action' => 'index'
            ));
        }
        
        $this->loadModel('Movimento');
        $this->loadModel('Posicao');
        
        $movimentos = $this->Movimento->find('all', array(
            'fields' => array( 'SUM(Movimento.valor) AS valor', 'YEAR(created) AS ano', 'MONTH(created) AS mes' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created) >='=>Date('Y')-1 ),
            'order' => array('created ASC'),
            'group' => array('YEAR(created)', 'MONTH(created)'),
            ) );
        $resultado  = '[';
        foreach ($movimentos as $movimento) {
            $resultado .= '[Date.UTC(' . $movimento[0]['ano'] . ',' . ($movimento[0]['mes']-1) . '),' . $movimento[0]['valor'] . '],';
        }
        $this->set('saldos', $resultado . ']');
        
        $gain_semana_long  = '[';
        $loss_semana_long  = '[';
        $gain_semana_short = '[';
        $loss_semana_short = '[';
        for($dia = 1; $dia < 6; $dia++) {
            $gain_semana_long .= $this->getMediaSemana($conta, $dia, 1, 'B') . ',';
            $loss_semana_long .= $this->getMediaSemana($conta, $dia, -1, 'B') . ',';
            $gain_semana_short .= $this->getMediaSemana($conta, $dia, 1, 'S') . ',';
            $loss_semana_short .= $this->getMediaSemana($conta, $dia, -1, 'S') . ',';
        }
        $this->set('gain_semana_long', $gain_semana_long . ']');
        $this->set('loss_semana_long', $loss_semana_long . ']');
        $this->set('gain_semana_short', $gain_semana_short . ']');
        $this->set('loss_semana_short', $loss_semana_short . ']');
        
        $this->set('top_gain', $this->getTopStock($conta, 1) );
        $this->set('top_loss', $this->getTopStock($conta, -1) );
        
        $this->set('setup_manha', $this->getSetupPeriodo($conta) );
        $this->set('setup_tarde', $this->getSetupPeriodo($conta, 'T') );
        
        $this->set('tipo_manha', $this->getTipoPeriodo($conta) );
        $this->set('tipo_tarde', $this->getTipoPeriodo($conta, 'T') );
    }
        
    private function getSetupPeriodo($conta, $periodo = 'M') {
        $this->Posicao->unbindModel(array('hasMany' => array('Ticket')));
        $this->Posicao->unbindModel(array('belongsTo' => array('Conta', 'Acao')));
        $posicao    = $this->Posicao->find('all', array(
            'fields' => array( '( COUNT(Posicao.id)*100 / (SELECT COUNT(p.id) FROM posicoes p where p.conta_id = ' . $conta['Conta']['id'] . ' and p.periodo = "' . $periodo . '" and p.liquido > 0 ) ) AS Percentual',
                               'TIME_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(Posicao.tempo))), "%H:%i:%s") AS Tempo', 'Categoria.nome', ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'periodo'=>$periodo, 'liquido > 0' ),
            'group' => array( 'Posicao.categoria_id' ),
            'order' => array( 'Percentual DESC'),
            'limit' => 6
            ) );
        return $posicao;
    }
    
    private function getTipoPeriodo($conta, $periodo = 'M') {
        $this->Posicao->unbindModel(array('hasMany' => array('Ticket')));
        $this->Posicao->unbindModel(array('belongsTo' => array('Conta', 'Acao')));
        $posicao    = $this->Posicao->find('all', array(
            'fields' => array( '( COUNT(Posicao.id)*100 / (SELECT COUNT(p.id) FROM posicoes p where p.conta_id = ' . $conta['Conta']['id'] . ' and p.periodo = "' . $periodo . '" and p.liquido > 0 ) ) AS Percentual', 'Posicao.tipo', ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'periodo'=>$periodo, 'liquido > 0' ),
            'group' => array( 'Posicao.tipo' )
            ) );
        return $posicao;
    }

    private function getMediaSemana($conta, $dia, $tipo, $subtipo) {
        $resultado  = ($tipo < 0) ? ' < ' : ' > ';
        $media      = ($tipo < 0) ? '(AVG(Posicao.liquido)) * -1' : 'AVG(Posicao.liquido)';
        
        $this->Posicao->unbindModel(array('hasMany' => array('Ticket')));
        $posicao    = $this->Posicao->find('first', array(
            'fields' => array( $media . ' AS media' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'liquido' . $resultado => 0, 'dia'=>$dia, 'tipo'=>$subtipo )
            ) );
        return ($posicao[0]['media'] != null) ? number_format( $posicao[0]['media'], 0, '.', '') : 0;
    }
    
    private function getTopStock($conta, $tipo) {
        $resultado  = ($tipo < 0) ? ' < ' : ' > ';
        $erro       = ($tipo < 0) ? ' > ' : ' < ';
        $media      = ($tipo < 0) ? '(AVG(Posicao.liquido)) * -1' : 'AVG(Posicao.liquido)';
        
        $this->Posicao->unbindModel(array('hasMany' => array('Ticket')));
        return $this->Posicao->find('all', array(
            'fields' => array( $media . ' AS soma', 'COUNT(Posicao.acao_id) AS acerto', '(SELECT COUNT(p.acao_id) FROM posicoes p where p.conta_id = ' . $conta['Conta']['id'] . ' and p.liquido ' . $erro . ' 0 and p.acao_id=Posicao.acao_id ) AS erro', 'Acao.sigla' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'liquido' . $resultado => 0 ),
            'group' => array( 'Posicao.acao_id' ),
            'order' => array( 'soma DESC'),
            'limit' => 6
            ) );
    }


    private function getUltimosMeses($qtd) {
        $datas  = array();
        $agora  = new DateTime('now');
        $agora->sub(new DateInterval("P" . ($qtd-1) . "M"));
        
        $datas['meses'][] = $agora->format('m');
        $datas['anos'][] = $agora->format('Y');
        $datas['siglas'][] = $this->getSiglaMes($agora->format('m'));
        
        for($i=0; $i<($qtd-1); $i++) {
            $agora->add(new DateInterval("P1M"));
            $datas['meses'][] = $agora->format('m');
            $datas['anos'][] = $agora->format('Y');
            $datas['siglas'][] = $this->getSiglaMes($agora->format('m'));
        }                
        return $datas;
    }
    
    private function getSiglaMes($mes) {
        switch ($mes) {
                case "01": return 'Jan';
                case "02": return 'Fev';
                case "03": return 'Mar';
                case "04": return 'Abr';
                case "05": return 'Mai';
                case "06": return 'Jun';
                case "07": return 'Jul';
                case "08": return 'Ago';
                case "09": return 'Set';
                case "10": return 'Out';
                case "11": return 'Nov';
                case "12": return 'Dez';
         }
    }
    
    public function pedido() {
        
    }
    
    public function investimento() {
        $this->layout   = 'investimento';
        
    }
    
}

?>