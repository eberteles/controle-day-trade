<?php

App::import('Helper', 'CakePtbr.Formatacao');

class EntradasController extends AppController {
    
    public $uses = array();
    
    public function index() {
        
        if (isset($this->data['Entrada']['entradas'])) {
            $this->loadModel('Posicao');
            
            $convert = explode("\n", utf8_encode($this->data['Entrada']['entradas']));

            $posicoes   = array();
            foreach ($convert as $linha) {                
                $posicoes[] = $this->lerLinha($linha);
            }
            $this->ordenarResultado($posicoes);

            $posicoesProntas    = array();
            while (count($posicoes)>0) {
                $posicao    = $this->getPosicao($posicoes);
                $posicoesProntas[]  = $posicao;
            }
            $this->Session->write('posicoesProntas', $posicoesProntas);
            
            $this->redirect(array('action' => 'confirmar'));
        }
       
    }
    
    public function confirmar() {
        //debug($this->Session->read('posicoesProntas'));
        if (isset($this->data['Entrada']['categoria'])) {
            $categorias = $this->data['Entrada']['categoria'];
            $notas      = $this->data['Entrada']['nota'];
            $posicoes   = $this->Session->read('posicoesProntas');
            $conta      = $posicoes[0]['conta'];
            if (isset($this->data['Conta'])) {
                $conta['Conta']                 = $this->data['Conta'];
                //$conta['Conta']['atualizacao']  = $posicoes[0]['Posicao']['created'];
                $conta['Conta']['acumulado']    = 0;
                $conta['Conta']['usuario_id']   = $this->Auth->user('id');
                $conta['Conta']['favorita']     = 'S';
                $this->addConta($conta, $posicoes[0]['Posicao']['created']);
            }
            $saldoDia   = 0;
            
            foreach ($posicoes as $key => $posicao) {
                
                $posicoes[$key]['Posicao']['conta_id']      = $conta['Conta']['id'];
                $posicoes[$key]['Posicao']['categoria_id']  = $categorias[$key];
                $posicoes[$key]['Posicao']['comentario']    = $notas[$key];
                $posicoes[$key]['Posicao']['comissao']      = 0;
                $posicoes[$key]['Posicao']['taxa']          = 0;
                
                foreach ($posicao['Ticket'] as $ticket) {
                    $posicoes[$key]['Posicao']['comissao'] += $this->getValorComissao($ticket['share'], $conta['Conta']['comissao'], $conta['Conta']['comissao_minima']);
                    $posicoes[$key]['Posicao']['taxa']     += $ticket['share'] * $conta['Conta']['taxa'];
                }
                $posicoes[$key]['Posicao']['liquido']       = $posicoes[$key]['Posicao']['total'] - ($posicoes[$key]['Posicao']['comissao'] + $posicoes[$key]['Posicao']['taxa']);
                $posicoes[$key]['Posicao']['crescimento']   = ($posicoes[$key]['Posicao']['liquido'] * 100) / $conta['Conta']['saldo'];
                $saldoDia += $posicoes[$key]['Posicao']['liquido'];
            }

            $this->savePosicoes($posicoes);
            $this->updAcumulado($conta, $saldoDia, $posicoes[0]['Posicao']['created']);
            $this->Session->setFlash('Operações salvas com sucesso!', 'default', array('class'=>'message success'));
            $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
            
        } else {
            $this->loadModel('Categoria');
            $categorias = $this->Categoria->find('list', array(
                    'conditions' => array(
                        'Categoria.usuario_id'=>$this->Auth->user('id')
                        )
                ));
            $this->set('categorias', $categorias);
        }
    }
    
    private function getValorComissao($share, $vlComissao, $vlMinimo) {
        $comissao   = floatval($share) * floatval($vlComissao);
        return ( $comissao < floatval($vlMinimo) ) ? floatval($vlMinimo) : $comissao;
    }

    private function getPosicao(&$posicoes){
        $conta  = $this->getConta($posicoes[0][7]);
        
        $dia        = date('w', strtotime(substr($posicoes[0][0], 6, 4) . '-' . substr($posicoes[0][0], 3, 2) . '-' . substr($posicoes[0][0], 0, 2) ));
        $periodo    = (substr($posicoes[0][1], 0, 2) < 13) ? 'M' : 'T';
        $totalShare = ($posicoes[0][3] == 'S') ? ($posicoes[0][5] * -1) : $posicoes[0][5];
        $comissao   = ($conta != false) ? ( $this->getValorComissao((float)$posicoes[0][5], $conta['Conta']['comissao'], $conta['Conta']['comissao_minima']) ) : 0;
        $taxa       = ($conta != false) ? ( (float)$posicoes[0][5] * $conta['Conta']['taxa'] ) : 0;
        $total      = ($posicoes[0][3] == 'S') ? $posicoes[0][5] * $posicoes[0][4] : ($posicoes[0][5] * $posicoes[0][4] * -1);
        
        $posicao    = array(
            'totalShare'=>$totalShare,
            'nuConta'=>$posicoes[0][7], 
            'conta'=>$conta, 
            'acao'=>$posicoes[0][2], 
            //'saldo'=>$conta['Conta']['saldo'],
            'Posicao' => array(
                //'conta_id' => $conta['Conta']['id'], 
                'acao_id'=>$this->getAcao($posicoes[0][2]), 'market'=>$this->data['Entrada']['market'], 
                'tipo'=>$posicoes[0][3], 'dia'=> $dia, 'periodo'=>$periodo, 'created'=>$posicoes[0][0] . ' ' . $posicoes[0][1], 
                'total' => $total, 'comissao' => $comissao, 'taxa' => $taxa, 'entrada'=>$posicoes[0][1]
                ));
        $posicao['Ticket'][] = array('share'=>$posicoes[0][5], 'time'=>$posicoes[0][1], 'preco'=>$posicoes[0][4], 'tipo'=>$posicoes[0][3]);
        
        unset($posicoes[0]);
        
        foreach ($posicoes as $key => $linha) {
            if($posicao['totalShare']==0) {
                break;
            }
            if($linha[2] == $posicao['acao']) {
                $totalShare += ($linha[3] == 'S') ? ($linha[5] * -1) : $linha[5];
                $total += ($linha[3] == 'S') ? $linha[5] * $linha[4] : ($linha[5] * $linha[4] * -1);
                $posicao['totalShare']  = $totalShare;
                $posicao['Posicao']['total']      = $total;
                $posicao['Posicao']['comissao']  += ($conta != false) ? ( $this->getValorComissao((float)$linha[5], $conta['Conta']['comissao'], $conta['Conta']['comissao_minima']) ) : 0;
                $posicao['Posicao']['taxa']      += ($conta != false) ? ( (float)$linha[5] * (float)$conta['Conta']['taxa'] ) : 0;
                $posicao['Posicao']['tempo']      = $this->getDifTime($linha[0], $posicao['Posicao']['entrada'], $linha[1]);
                $posicao['Ticket'][]    = array('share'=>$linha[5], 'time'=>$linha[1], 'preco'=>$linha[4], 'tipo'=>$linha[3]);
                unset($posicoes[$key]);
            }
        }
        
        //Verificar resultado P/N / total
        $posicao['Posicao']['resultado'] = (($total - $posicao['Posicao']['comissao'] - $posicao['Posicao']['taxa']) < 0) ? 'N' : 'P';
        
        $posicoes = array_values($posicoes);
        return $posicao;
    }
    
    private function getDifTime($data, $entrada, $saida) {
        $dtEntrada  = DateTime::createFromFormat('d/m/Y H:i:s', $data . ' ' . $entrada);
        $dtSaida    = DateTime::createFromFormat('d/m/Y H:i:s', $data . ' ' . $saida);

        $diff = $dtEntrada->diff($dtSaida);
        return $diff->format("%H:%I:%S");
    }


    private function ordenarResultado(&$res) {
        //percorre as chaves e valores
        foreach ($res as $key => $row) {
            $ndata[$key] = $row[0];
            //Inverte a data
            $ndata[$key] = implode("/",array_reverse(explode("/",$ndata[$key])));
            $hora[$key] = $row[1];
        }
        //ordena
        array_multisort($ndata, SORT_ASC, $hora, SORT_ASC,$res);
    }
    
    private function lerLinha($linha) {
        $posicao = array();
        $posicao[] = $this->data['Entrada']['data'];
                
        $trade   = explode("\t", $linha);
        foreach ($trade as $coluna) {
            if($coluna == '')continue;
            $posicao[] = $coluna;
        }
        return $posicao;
    }
    
    private function getAcao($sigla) {
        $this->loadModel('Acao');
        $acaoDb = $this->Acao->find('first', array(
                'fields' => array('Acao.id'),
                'conditions' => array(
                    'Acao.sigla'=>$sigla
                    )
            ));
        if(isset($acaoDb['Acao']['id']) && $acaoDb['Acao']['id'] > 0) {
            return $acaoDb['Acao']['id'];
        }
        else {
            $this->Acao->create();
            $this->Acao->save(array('Acao'=>array('sigla'=>$sigla)));
            return $this->Acao->id;
        }
    }
    
    private function getConta($sigla) {
        $this->loadModel('Conta');
        $contaDb    = $this->Conta->find('first', array(
                'conditions' => array(
                    'Conta.usuario_id'=>$this->Auth->user('id'),
                    'Conta.sigla'=>$sigla
                    )
            ));
        if(isset($contaDb['Conta']['id']) && $contaDb['Conta']['id'] > 0) {
            return $contaDb;
        }
        else {
            return false;
        }
    }
    
    private function addConta(&$conta) {
        $this->loadModel('Conta');
        $this->Conta->create();
        $this->Conta->save($conta);
        $conta = $this->Conta->read(null,$this->Conta->id);
        $this->Session->write('conta', $conta);
    }
    
    private function savePosicoes($posicoes) {
        $this->loadModel('Posicao');
        $this->Posicao->create();
        $this->Posicao->saveAll($posicoes, array('deep' => true));
    }
    
    private function updAcumulado($conta, $saldoDia, $atualizacao) {
        $valorMovimento = ($conta['Conta']['saldo']+$conta['Conta']['acumulado']+$saldoDia);
        
        if(!isset($conta['Conta']['atualizacao']) || $conta['Conta']['atualizacao'] == null) {  //Conta Nova
            $conta['Conta']['acumulado']    = $saldoDia;
            $conta['Conta']['atualizacao']  = $atualizacao;
            $fieldList = array('atualizacao', 'acumulado');
        } else {
            $mesAgora   = date('n', strtotime(substr($atualizacao, 6, 4) . '-' . substr($atualizacao, 3, 2) . '-' . substr($atualizacao, 0, 2) ));
            $mesConta   = date('n', strtotime(substr($conta['Conta']['atualizacao'], 6, 4) . '-' . substr($conta['Conta']['atualizacao'], 3, 2) . '-' . substr($conta['Conta']['atualizacao'], 0, 2) ));
            if($mesAgora != $mesConta) { //Mês diferente
                $conta['Conta']['saldo']       += $conta['Conta']['acumulado'];
                $conta['Conta']['atualizacao']  = $atualizacao;
                $conta['Conta']['acumulado']    = $saldoDia;
                $fieldList = array('atualizacao', 'saldo', 'acumulado');
            } else {
                $valorMovimento = $saldoDia;
                $conta['Conta']['acumulado'] += $saldoDia;
                $fieldList = array('acumulado');
            }
        }
        
        $this->loadModel('Conta');
        $this->loadModel('Posicao');
        
        $this->Conta->Movimento->save(array('conta_id'=>$conta['Conta']['id'], 'valor'=>$valorMovimento, 'created'=>$atualizacao));
        
        $this->Conta->id = $conta['Conta']['id'];
        $this->Conta->save($conta, array('fieldList' => $fieldList));
        
        $movimentos = $this->Conta->Movimento->find('all', array(
            'fields' => array( 'SUM(Movimento.valor) AS valor', 'YEAR(created) AS ano', 'MONTH(created) AS mes' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'] ),
            'order' => array('created ASC'),
            'group' => array('YEAR(created)', 'MONTH(created)'),
            'limit' => 8,
            ) );
        $posicoes_ano   = $this->Posicao->find('all', array(
            'fields' => array( 'SUM(Posicao.liquido) AS valor', 'YEAR(created) AS ano', 'MONTH(created) AS mes' ),
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)'=>Date('Y') ),
            'order' => array('created ASC'),
            'group' => array('YEAR(created)', 'MONTH(created)'),
            ) );

        $acerto_ano   = $this->Posicao->find('count', array(
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)'=>Date('Y'), 'Posicao.liquido > '=>0 ),
            ) );
        $erro_ano     = $this->Posicao->find('count', array(
            'conditions' => array( 'conta_id'=>$conta['Conta']['id'], 'YEAR(created)'=>Date('Y'), 'Posicao.liquido < '=>0 ),
            ) );

        $this->Session->write('conta', $conta);
        $this->Session->write('movimentos', $movimentos);
        $this->Session->write('posicoes_ano', $posicoes_ano);
        $this->Session->write('acerto_ano', ($acerto_ano*100)/($erro_ano+$acerto_ano));
    }


    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'assinante' || $user['role'] === 'funcionario') {
                return true;
            }
        }
        $this->redirect($this->Auth->redirect());
    }
    
}

?>