<?php

class ContasController extends AppController {
    
    
    public function index() {
        
        if (isset($this->data['Conta']['id'])) {
            $this->Conta->create();
            if ($this->Conta->save($this->request->data)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Não foi possível salvar. Tente novamente.');
            }
        }
        
        $conditions = array('usuario_id'=>$this->Auth->user('id'));
        
        if (isset($this->data['pesquisar']) && $this->data['pesquisar'] != '') {
            $pesquisa   = mb_strtoupper($this->data['pesquisar'], 'UTF-8');
            $conditions['descricao like'] = '%' . $pesquisa . '%';
            $this->set('pesquisar', $pesquisa);
           
        }
        
        $this->set('contas', $this->Conta->find('all', array(
            'conditions' => $conditions,
            'order' => 'modified ASC'
        )));
    }
    
    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'assinante') {
                return true;
            }
        }
        $this->redirect($this->Auth->redirect());
    }
    
}

?>