<?php

class CategoriasController extends AppController {
    
    
    public function index() {
        
        if (isset($this->data['Categoria']['id'])) {
            $this->Categoria->create();
            if ($this->Categoria->save($this->request->data)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Não foi possível salvar. Tente novamente.');
            }
        }
        
        $conditions = array('usuario_id'=>$this->Auth->user('id'));
        
        if (isset($this->data['pesquisar']) && $this->data['pesquisar'] != '') {
            $pesquisa   = mb_strtoupper($this->data['pesquisar'], 'UTF-8');
            $conditions['nome like'] = '%' . $pesquisa . '%';
            $this->set('pesquisar', $pesquisa);
           
        }
        
        $this->set('categorias', $this->Categoria->find('all', array(
            'conditions' => $conditions,
            'order' => 'nome ASC'
        )));
    }
    
    public function delete()
    {
        if (isset($this->data['Categoria']['id'])) {
            if ($this->Categoria->delete($this->data['Categoria']['id'])) {
                $this->Session->setFlash('Registro excluído com sucesso', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Erro ao excluir o registro');
            }
        } else {
            $this->Session->setFlash('Não foi possível excluir o registro');
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'assinante') {
                return true;
            }
        }
        $this->redirect($this->Auth->redirect());
    }
    
}

?>