<?php

App::import('Helper', 'CakePtbr.Formatacao');

class CalculadoraController extends AppController {
    
    public $uses = array();
    
    public function index() {
        $formatacao = new FormatacaoHelper(new View());
        $diasUteis  = $formatacao->diasUteis(date('m'),date('Y'));
        
        $conta  = $this->Session->read('conta');

        //$this->set('loss', number_format(($conta['Conta']['saldo']+$conta['Conta']['acumulado'])/$diasUteis, 2, '.', '') );
        $this->set('loss', number_format(($conta['Conta']['risco']/100)*($conta['Conta']['saldo']+$conta['Conta']['acumulado']), 2, '.', '') );
        $this->set('saldo', number_format( $conta['Conta']['saldo'] + $conta['Conta']['acumulado'], 2, '.', '') );
        $this->set('risco', number_format( $conta['Conta']['risco']/100, 2, '.', '') );
        $this->set('bp', number_format( ($conta['Conta']['saldo']+$conta['Conta']['acumulado'])*$conta['Conta']['dbp'], 2, '.', '') );
        $this->set('comissao', $conta['Conta']['comissao']);
        $this->set('comissao', $conta['Conta']['comissao']);
        $this->set('cm_minima', $conta['Conta']['comissao_minima']);
        $this->set('taxa', $conta['Conta']['taxa']);
    }    
}

?>