<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    public $components = array(
        'Flash',
        'Session',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'dashboard', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'usuarios', 'action' => 'login'),
            'authorize' => array('Controller')
        )
    );
    
    public $helpers = array('Tabela', 'Funcoes', 'CakePtbr.Formatacao');
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        $this->Auth->authenticate = array(AuthComponent::ALL => array('userModel' => 'Usuario', 'contain'  => 'Cliente'), 'Form');
        
    }
        
    public function isAuthorized($user) {
        if (isset($user['role']) && ($user['role'] === 'admin' || $user['role'] === 'gerente' || $user['role'] === 'funcionario' || $user['role'] === 'assinante')) {
            return true;
        }
        return false;
    }
    
    public function delete()
    {
        if (isset($this->data[$this->name]['id'])) {
            if ($this->$this->name->delete($this->data[$this->name]['id'])) {
                $this->Session->setFlash('Registro excluído com sucesso', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Erro ao excluir o registro');
            }
        } else {
            $this->Session->setFlash('Não foi possível excluir o registro');
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
}
