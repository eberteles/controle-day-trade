<?php

class DataPtBrBehavior extends ModelBehavior {

    public function beforeValidate(Model $model, $options = array()) {
        foreach ( $model->_schema as $field => $attr ) {
            switch ( $attr[ 'type' ] ) {
                case 'date':
                    $this->formateDate( $model->data[ $model->name ][ $field ] );
                    break;
                case 'datetime':
                    $this->formateDateTime( $model->data[ $model->name ][ $field ] );
                    break;
            }
        }
        return true;
    }
    
    public function afterFind(Model $model, $results, $primary = false) {

    
        if(is_array($model->_schema)) {

            foreach ( $model->_schema as $field => $attr ) {

                switch ( $attr[ 'type' ] ) {
                        case 'date':
                                $function = "data";
                                break;
                        case 'datetime':
                                $function = "dataHora";
                                break;
                        default:
                                $function = "";
                                break;
                }
                if ( !empty( $function ) ) {
                        if ( isset( $results[ $model->name ][ $field ] ) ) {

                                self::$function( $results[ $model->name ][ $field ] );
                        } else {
                                foreach ( $results as $key => $out ) {
                                    if ( isset( $results[ $key ] ) && isset( $results[ $key ][ $model->name ] ) && isset( $results[ $key ][ $model->name ][ $field ] ) ) {
                                            self::$function( $results[ $key ][ $model->name ][ $field ] );
                                        }
                                }
                        }
                }
            }
        }

        return $results;
    }
    
    /**
     * Retorna a data no formato brasileiro
     */
    static function data( &$sInput )
    {
            $sInput = strtotime( $sInput );

            if ( empty( $sInput ) ) {
                    return null;
            }
            $sInput = date( "d/m/Y", $sInput );

            return $sInput;
    }

    /**
     * Retorna a data e hora no formato brasileiro
     */
    static function dataHora( &$sInput )
    {
            $sInput = strtotime( $sInput );

            if ( empty( $sInput ) ) {
                    return null;
            }
            $sInput = date( "d/m/Y H:i:s", $sInput );

            return $sInput;
    }
    
    /**
     * Converte a data para o formato americano
     *
     * @param unknown_type $data
     * @return unknown
     */
    function formateDate( &$data )
    {
        if ( strstr( $data, "/" ) ) {
            $d = explode( "/", $data );
            $rstData = "$d[2]-$d[1]-$d[0]";
            $data = $rstData;
        }
        return $data;
    }

    /**
     * Converte a data para o formato americano
     *
     * @param unknown_type $data
     * @return unknown
     */
    function formateDateTime( &$data )
    {
        if ( strstr( $data, "/" ) ) {
            $time = substr( $data, 11, 8 );
            $date = substr( $data, 0, 10 );

            $data = $this->formateDate( $date );

            if ( $time ) {
                $data = $data . ' ' . $time;
            }
        }

        return $data;
    }

    
}