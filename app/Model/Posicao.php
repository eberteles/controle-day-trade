<?php

class Posicao extends AppModel {
    public $name = 'Posicao';
    public $order = "created DESC";
    public $displayField = 'total';
    var $actsAs  = array('CakePtbr.AjusteFloat');
    
    public $hasMany = array(
        'Ticket' => array(
            'className' => 'Ticket',
            'foreignKey' => 'posicao_id'
        )
    );
    
    public $belongsTo = array(
        'Conta' => array(
            'className' => 'Conta',
            'foreignKey' => 'conta_id'
        ),
        'Acao' => array(
            'className' => 'Acao',
            'foreignKey' => 'acao_id'
        ),
        'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'categoria_id'
        )
    );
    
    public function beforeSave($options = array())
    {        
        return parent::beforeSave($options);
    }
    
}