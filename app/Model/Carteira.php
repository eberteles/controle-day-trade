<?php

class Carteira extends AppModel {
    public $name = 'Carteira';
    public $order = "sigla ASC";
    public $displayField = 'sigla';
    var $actsAs  = array('CakePtbr.AjusteFloat');
    
    public $validate = array(
        'sigla' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Sigla da Carteira.'
            )
        ),
        'descricao' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Descrição da Carteira.'
            )
        ),
        'meta' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Meta da Carteira.'
            )
        ),
        'saldo' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Saldo da Carteira.'
            )
        ),
        'dbp' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o DBP da Carteira.'
            )
        )
    );
    
    public $hasMany = array(
        'Movimento' => array(
            'className' => 'Movimento',
            'foreignKey' => 'conta_id'
        )
    );
    
    public function beforeSave($options = array())
    {
        if(isset($this->data[$this->name]['sigla'])) {
            $this->data[$this->name]['sigla']   = mb_strtoupper($this->data[$this->name]['sigla'], 'UTF-8');
        }
        
        return parent::beforeSave($options);
    }
    
}