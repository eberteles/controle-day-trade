<?php

class Categoria extends AppModel {
    public $name = 'Categoria';
    public $displayField = 'nome';
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o nome da categoria.'
            )
        )
    );
    
    public function beforeSave($options = array())
    {        
        return parent::beforeSave($options);
    }
}