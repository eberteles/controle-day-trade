<?php

class Acao extends AppModel {
    public $name = 'Acao';
    public $order = "sigla ASC";
    public $displayField = 'sigla';
    public $useTable = 'acoes';


    public $validate = array(
        'sigla' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Sigla da Ação.'
            )
        )
    );
    
    public function beforeSave($options = array())
    {
        $this->data[$this->name]['sigla']        = mb_strtoupper($this->data[$this->name]['sigla'], 'UTF-8');
        
        return parent::beforeSave($options);
    }
    
}