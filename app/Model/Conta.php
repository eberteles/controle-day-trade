<?php

class Conta extends AppModel {
    public $name = 'Conta';
    public $order = "sigla ASC";
    public $displayField = 'sigla';
    var $actsAs  = array('CakePtbr.AjusteFloat');
    
    public $validate = array(
        'sigla' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Sigla da Conta.'
            )
        ),
        'descricao' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Descrição da Conta.'
            )
        ),
        'meta' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Meta da Conta.'
            )
        ),
        'saldo' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Saldo da Conta.'
            )
        ),
        'dbp' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o DBP da Conta.'
            )
        )
    );
    
    public $hasMany = array(
        'Movimento' => array(
            'className' => 'Movimento',
            'foreignKey' => 'conta_id'
        )
    );
    
    public function beforeSave($options = array())
    {
        if(isset($this->data[$this->name]['sigla'])) {
            $this->data[$this->name]['sigla']   = mb_strtoupper($this->data[$this->name]['sigla'], 'UTF-8');
        }
        
        return parent::beforeSave($options);
    }
    
}